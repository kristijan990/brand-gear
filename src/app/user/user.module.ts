import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user.component';
import { UserDetaisViewComponent } from './user-detais-view/user-detais-view.component';
import { CampaignsOverviewComponent } from './user-detais-view/campaigns-overview/campaigns-overview.component';
import { CompanyDetailsComponent } from './user-detais-view/company-details/company-details.component';
import { DetailsOverviewComponent } from './user-detais-view/details-overview/details-overview.component';
import { EditDetailsComponent } from './user-detais-view/edit-details/edit-details.component';
import { SecurityComponent } from './user-detais-view/security/security.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { UserRoutingModule } from './user-routing.module';
import { MatSelectModule } from '@angular/material/select';
import { CustomerTableComponent } from '../customer/customer-details/customer-table/customer-table.component';
import { CustomerModule } from '../customer/customer.module';
import { CampaignModule } from '../campaign/campaign.module';
import { BusinessModule } from '../business/business.module';




@NgModule({
  declarations: [
    UserComponent,
    UserDetaisViewComponent,
    CampaignsOverviewComponent,
    CompanyDetailsComponent,
    DetailsOverviewComponent,
    EditDetailsComponent,
    SecurityComponent,

  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    UserRoutingModule,
    MatSelectModule,
    CustomerModule,
    CampaignModule,
    BusinessModule

  ]
})
export class UserModule { }
