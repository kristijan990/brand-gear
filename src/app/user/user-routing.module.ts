import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { CampaignsOverviewComponent } from "./user-detais-view/campaigns-overview/campaigns-overview.component";
import { CompanyDetailsComponent } from "./user-detais-view/company-details/company-details.component";
import { DetailsOverviewComponent } from "./user-detais-view/details-overview/details-overview.component";
import { EditDetailsComponent } from "./user-detais-view/edit-details/edit-details.component";
import { SecurityComponent } from "./user-detais-view/security/security.component";
import { UserDetaisViewComponent } from "./user-detais-view/user-detais-view.component";
import { UserComponent } from "./user.component";

const userRoutes: Routes = [
  {
    path: '',
    component: UserComponent,
    children: [
      {
        path: 'details',
        component: UserDetaisViewComponent,
        children: [
          {
            path: 'overview',
            component: DetailsOverviewComponent
          },
          {
            path: 'edit-details',
            component: EditDetailsComponent
          },
          {
            path: 'edit-company',
            component: CompanyDetailsComponent
          },
          {
            path: 'security',
            component: SecurityComponent
          },
          {
            path: 'campaigns-overview',
            component: CampaignsOverviewComponent
          },
          {
            path: '**',
            redirectTo: 'user',
          },
        ]
      },



    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(userRoutes)],
  exports: [RouterModule],
})
export class UserRoutingModule {}
