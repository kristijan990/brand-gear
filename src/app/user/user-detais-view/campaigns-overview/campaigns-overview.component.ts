import { Component, OnInit } from '@angular/core';

import { Subject, take, takeUntil } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { AplicantObject } from 'src/app/customer/customer.model';
import { CustomerService } from 'src/app/customer/customer.service';
import { SharedService } from 'src/app/shared/shared.service';
import { CompanyDetails } from '../../user.model';
import { UserService } from '../../user.service';


@Component({
  selector: 'app-campaigns-overview',
  templateUrl: './campaigns-overview.component.html',
  styleUrls: ['./campaigns-overview.component.scss']
})
export class CampaignsOverviewComponent implements OnInit {

  accountId: string = null;
  accountGuid: string = null;
  has_aplications: Boolean = false;
  aplicantObject: AplicantObject = new AplicantObject();
  showSpinner: Boolean = false;
  is_company: Boolean = false;
  companyId: string = null;
  private _unsubscribeAll: Subject<void> = new Subject<void>();
  companyObject: CompanyDetails = new CompanyDetails();


  constructor(
               private _authService: AuthService,
               private _customerService: CustomerService,
               private _sharedService: SharedService,
               private _userService: UserService
  ) { }

  ngOnInit(): void {
    this.showSpinner = true;

    this.accountId = this._authService.currentActiveAccount.account.accountId;
    this.accountGuid = this._authService.currentActiveAccount.account.guid;
    this.checkAccountType();

    //  this.getCustomerDetails();
    // this.getCompanyDetails();



  }

  getCustomerDetails() {


    this._customerService.getCustomerGuid(this.accountGuid).pipe(takeUntil(this._unsubscribeAll)).subscribe(
      (data: AplicantObject) => {
         this.aplicantObject = data;
         this.accountId = this.aplicantObject.accountId;
         console.log('this.aplicantObject -> ', this.aplicantObject);
         if(this.aplicantObject.campaignApplications == true) {

           this.has_aplications = true;
         } else {
           this.has_aplications = false;
         }
         this.showSpinner = false;
      }
    )
  }

  getCompanyDetails() {

    this._userService.getCompanyDetails(this.accountId).pipe(takeUntil(this._unsubscribeAll)).subscribe(
      (data: CompanyDetails) => {
        this.companyObject = data;
        this.companyId = data.companyId;
        console.log("company Id--->", this.companyId)
        console.log("company object--->", this.companyObject)
      }


    )
  }

  checkAccountType(){
    this._sharedService.is_comapny.pipe(takeUntil(this._unsubscribeAll)).subscribe(
   (data: Boolean) => {
     this.is_company = data;
     if(this.is_company) {
       this.getCompanyDetails();
       this.showSpinner = false;
     } else {
       this.getCustomerDetails();
     }

   }

 )



 }




}
