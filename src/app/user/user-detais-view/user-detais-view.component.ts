import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { Account } from 'src/app/auth/auth.model';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-user-detais-view',
  templateUrl: './user-detais-view.component.html',
  styleUrls: ['./user-detais-view.component.scss']
})
export class UserDetaisViewComponent implements OnInit, OnDestroy {

  private _unsubscribeAll: Subject<void> = new Subject<void>();
   currentActiveAccountFullName: string = null;
   currentActiveAccountMail: string = null;
   currentActivProfilePicture: string = null;

constructor(
                private _authService: AuthService,
                private router: Router,
  ) { }

  ngOnInit(): void {
    this.getAccountDetails();
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.complete();
    this._unsubscribeAll.next();
  }


  getAccountDetails() {
    this._authService.currentActiveAccountSubject.pipe(takeUntil(this._unsubscribeAll)).subscribe(
      (data: Account) => {
        this.currentActiveAccountFullName = data.account.firstName + ' ' + data.account.lastName;
        this.currentActiveAccountMail = data.account.email;
        this.currentActivProfilePicture = data.account.profilePicture;

        console.log("profilePicture", this.currentActivProfilePicture)
      }
    );
  }

}
