import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Subject, takeUntil } from 'rxjs';
import { AccountObject } from 'src/app/auth/auth.model';
import { AuthService } from 'src/app/auth/auth.service';
import { LookUpObject } from 'src/app/shared/lookup/lookup.model';
import { LookupService } from 'src/app/shared/lookup/lookup.service';
import { SharedService } from 'src/app/shared/shared.service';
import Swal from 'sweetalert2';

import {
  AccountDetailsObject,
  CustomerCreateDetails,
  CustomerDetailsObject,
  CustomerUpdateDetails,
  UpdateAccountObj,
} from '../../user.model';
import { UserService } from '../../user.service';

@Component({
  selector: 'app-edit-details',
  templateUrl: './edit-details.component.html',
  styleUrls: ['./edit-details.component.scss'],
})
export class EditDetailsComponent implements OnInit, OnDestroy {
  showSpinner: Boolean = false;
  is_editing: Boolean = false;
  is_saving_results: Boolean = false;
  customerObject: CustomerDetailsObject = new CustomerDetailsObject();
  accountTypeList: Array<LookUpObject> = new Array<LookUpObject>();
  private _unsubscribeAll: Subject<void> = new Subject<void>();
  accountEditForm: FormGroup;
  accountObject: AccountObject = new AccountObject();
  customerDetailsId: string = null;
  private unsubscribeAll: Subject<void> = new Subject<void>();

  constructor(
    private _lookupService: LookupService,
    private _userService: UserService,
    private _formBuilder: FormBuilder,
    private _authService: AuthService,
    private _router: Router,
    private _cookieService: CookieService,
    private _sharedService: SharedService
  ) {}

  ngOnInit(): void {
    this.accountObject = this._authService.currentActiveAccount.account;
    this.getGenderType();
    this.initAccountFormGroup();
    this.getCustomerDetails();
    this.isCompleted();
  }

  ngOnDestroy(): void {
    this.unsubscribeAll.complete();
    this.unsubscribeAll.next();
  }

  initAccountFormGroup() {
    return (this.accountEditForm = this._formBuilder.group({
      accountFirstName: [this.accountObject.firstName, Validators.required],
      accountLastName: [this.accountObject.lastName, Validators.required],
      accountEmail: [this.accountObject.email, Validators.required],
      accountGender: [this.accountObject.gender, Validators.required],
      accountPhoneNumber: [this.accountObject.phoneNumber, Validators.required],
      profilePicture: [this.accountObject.profilePicture, Validators.required],
      customerLocation: ['', Validators.required],
    }));
  }

  getCustomerDetails() {
    this._userService
      .getCustomerDetails(this._authService.currentActiveAccount.account.guid)
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe((data: CustomerDetailsObject) => {
        this.customerDetailsId = data.customerDetailsId;
        console.log('customerGuid', this.customerDetailsId);
      });
  }

  isCompleted() {
    this._sharedService.is_account_competed
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe((data: Boolean) => {
        this.is_editing = data;
      });
  }

  updateAccount() {
    if (this.is_editing) {
      let updateAccountObj = new UpdateAccountObj();

      updateAccountObj.firstName =
        this.accountEditForm.get('accountFirstName').value;
      updateAccountObj.lastName =
        this.accountEditForm.get('accountLastName').value;
      updateAccountObj.phoneNumber =
        this.accountEditForm.get('accountPhoneNumber').value;
      updateAccountObj.gender = this.accountEditForm.get('accountGender').value;
      updateAccountObj.profilePicture =
        this.accountEditForm.get('profilePicture').value;
      updateAccountObj.email = this.accountEditForm.get('accountEmail').value;
      updateAccountObj.accountId = this.accountObject.accountId;

      let updateCustomer = new CustomerUpdateDetails();

      updateCustomer.accountId = this.accountObject.accountId;
      updateCustomer.profilePicture =
        this.accountEditForm.get('profilePicture').value;
      updateCustomer.accountId =
        this._authService.currentActiveAccount.account.accountId;
      updateCustomer.location =
        this.accountEditForm.get('customerLocation').value;
      updateCustomer.customerDetailsId = this.customerDetailsId;

      this._userService
        .updateAccount(updateAccountObj)
        .pipe(takeUntil(this.unsubscribeAll))
        .subscribe((data: AccountObject) => {
          this.showSpinner = true;
        });

      this._userService
        .updateCustomer(updateCustomer)
        .pipe(takeUntil(this.unsubscribeAll))
        .subscribe((data: CustomerDetailsObject) => {
          Swal.fire({
            icon: 'success',
            title: 'Profile successfully updated !',
            showConfirmButton: false,
            timer: 2000,
            willClose: () => {
              this._router.navigate(['user/details/overview']);
              this.is_saving_results = false;
              this.showSpinner = false;
              this._sharedService.is_loaded.next(true);
            },
          });
        });
    } else {
      let updateAccountObject = new UpdateAccountObj();
      let customerCreateDetails = new CustomerCreateDetails();
      updateAccountObject.firstName =
        this.accountEditForm.get('accountFirstName').value;
      updateAccountObject.lastName =
        this.accountEditForm.get('accountLastName').value;
      updateAccountObject.phoneNumber =
        this.accountEditForm.get('accountPhoneNumber').value;
      updateAccountObject.profilePicture =
        this.accountEditForm.get('profilePicture').value;
      updateAccountObject.email =
        this.accountEditForm.get('accountEmail').value;
      updateAccountObject.gender =
        this.accountEditForm.get('accountGender').value;
      updateAccountObject.accountId =
        this._authService.currentActiveAccount.account.accountId;
      customerCreateDetails.location =
        this.accountEditForm.get('customerLocation').value;
      // customerCreateDetails.profilePicture = this.accountEditForm.get('profilePicture').value;
      customerCreateDetails.accountId =
        this._authService.currentActiveAccount.account.accountId;
      this._userService
        .createCustomer(customerCreateDetails)
        .pipe(takeUntil(this.unsubscribeAll))
        .subscribe((data: CustomerDetailsObject) => {
          if (data && data.accountId) {
            Swal.fire({
              icon: 'success',
              title: 'Profile successfully created !',
              showConfirmButton: false,
              timer: 2000,
              willClose: () => {
                this._router.navigate(['../../overview']);
                this.is_saving_results = false;
                this.showSpinner = false;
              },
            });
          }
        });
      this._userService
        .updateAccount(updateAccountObject)
        .pipe(takeUntil(this.unsubscribeAll))
        .subscribe((data: AccountObject) => {
          console.log('account', data);
        });
    }
  }

  getGenderType() {
    this._lookupService
      .lookUpGenderList()
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((data: Array<LookUpObject>) => {
        console.log('Lookup =>', data);
        this.accountTypeList = data;
      });
  }

  /**
   * Function for Uploading Image
   */
  uploadImage(event: any): void {
    console.log('file --> ', event);
    if (event.target.files.length > 0) {
      this.showSpinner = true;

      const file: File = (
        (event.target as HTMLInputElement).files as FileList
      )[0];

      let form_data: FormData = new FormData();
      form_data.append('file', file, file.name);
      console.log('file --> ', file);

      this._userService
        .mediaImage(form_data)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(
          (data: string) => {
            Swal.fire({
              icon: 'success',
              text: 'Photo is successfully uploaded!',
              showConfirmButton: false,
              timer: 2000,
              willClose: () => {
                console.log('photo url --> ', data);
                // this.patientForm.get('profilePicture')?.setValue(data);
                this.accountEditForm.get('profilePicture')?.setValue(data);
                console.log(
                  'profilePictore',
                  this.accountEditForm.get('ProfilePicture')
                );
                this.showSpinner = false;
                event.target.value = null;
              },
            });
          },
          () => {
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Something went wrong! Please try again',
              showConfirmButton: false,
              timer: 2000,
              willClose: () => {
                this.showSpinner = false;
                event.target.value = null;
              },
            });
          }
        );
    }
  }
}
