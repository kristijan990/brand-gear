import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Subject, takeUntil } from 'rxjs';
import { Account, AccountObject } from 'src/app/auth/auth.model';
import { AuthService } from 'src/app/auth/auth.service';
import { SharedService } from 'src/app/shared/shared.service';
import { CompanyDetails } from '../../user.model';
import { UserService } from '../../user.service';

@Component({
  selector: 'app-details-overview',
  templateUrl: './details-overview.component.html',
  styleUrls: ['./details-overview.component.scss']
})
export class DetailsOverviewComponent implements OnInit, OnDestroy {

  private _unsubscribeAll: Subject<void> = new Subject<void>();
  is_saving_results: Boolean = false;
  is_comapny: Boolean = false;
  activeAccountId: string;
  companyDetails: CompanyDetails = new CompanyDetails();
  accountObject: AccountObject = new AccountObject();



  constructor(
        private _userService: UserService,
        private _route: ActivatedRoute,
        private _router: Router,
        private _authService: AuthService,
        private _cookieService: CookieService,
        private _sharedService: SharedService
        ) {}

  ngOnInit(): void {
    this.checkAccountType();

      this.activeAccountId = this._authService.currentActiveAccount.account.accountId;

      if(this.is_comapny) {
        this.loadCompany(this.activeAccountId);

      }
      this.accountObject = this._authService.currentActiveAccount.account;


  }



  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  editCompany() {
    this._router.navigate(['/user/details/edit-company'], { queryParams: { companyId: this.companyDetails.companyId } });
  }

  loadCompany(accountId: string):void {

    if(this._userService.company_details.value.companyId == null) {
      this._userService.getCompanyDetails(accountId).pipe(takeUntil(this._unsubscribeAll)).subscribe(
        (data: CompanyDetails)  => {
          this.companyDetails = data;
          this._userService.company_details.next(this.companyDetails);
          console.log('USER: ', this.companyDetails);
        }
      )
    } else {
        this.companyDetails = this._userService.company_details.value;
    }

  }

  checkAccountType(){
   this._sharedService.is_comapny.pipe(takeUntil(this._unsubscribeAll)).subscribe(
  (data: Boolean) => {
    this.is_comapny = data;
  }
 )
}
}
