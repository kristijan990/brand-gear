import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject, takeUntil } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-security',
  templateUrl: './security.component.html',
  styleUrls: ['./security.component.scss']
})
export class SecurityComponent implements OnInit, OnDestroy {

  editPasswordFormGroup: FormGroup;
  new_password: Boolean = true;
  re_enter_password: Boolean = true;
  wrongPassword: Boolean = false;
  showSpinner: Boolean = false;
  activeAccountName: string = null;
  private _unsubscribeAll: Subject<void> = new Subject<void>();

  constructor(
              public _formBuilder: FormBuilder,
              private _authService: AuthService
  ) { }

  ngOnInit(): void {

    this.initPasswordFormGroup();
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.complete();
    this._unsubscribeAll.next();
  }

  initPasswordFormGroup() {

    return this.editPasswordFormGroup = this._formBuilder.group({

      oldPassword: [null, Validators.required],
      newPassword: [null, Validators.required],
    })
  }


  newPassword() {

    let accountId: string = this._authService.currentActiveAccount.account.accountId;
    let oldPassword: string = this.editPasswordFormGroup.get("oldPassword").value;
    let newPassword: string = this.editPasswordFormGroup.get("newPassword").value;


    console.log("accountId", accountId)

    this._authService.updatePassword(accountId, oldPassword, newPassword).pipe(takeUntil(this._unsubscribeAll)).subscribe(
      ((data) =>
       {
        if(data) {

          console.log("update password", data)
          Swal.fire({
            icon: 'success',
            title: 'Your update was successful!',
            showConfirmButton: false,
            timer: 3000,
            willClose: () => {
             },

          });
        }
      }),
      (error) => {
        if(error.error.response == "Account with that Id is not existing, or your old password is incorrect.") {
          let passwordWrong = document.getElementById('wrong-password');
          this.wrongPassword = true;
          passwordWrong.style.display = "block";

        }
      }
    )}


}
