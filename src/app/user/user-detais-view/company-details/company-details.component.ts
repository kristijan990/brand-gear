import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { SharedService } from 'src/app/shared/shared.service';
import Swal from 'sweetalert2';
import { CompanyDetails, CreateCompany, UpdateCompany } from '../../user.model';
import { UserService } from '../../user.service';

@Component({
  selector: 'app-company-details',
  templateUrl: './company-details.component.html',
  styleUrls: ['./company-details.component.scss']
})
export class CompanyDetailsComponent implements OnInit, OnDestroy {

  is_editing: Boolean;
  is_saving_results :Boolean;
  showSpinner: boolean = false;
  companyObject: CompanyDetails = new CompanyDetails();
  private _unsubscribeAll: Subject<void>  = new Subject<void>();
  companyFormGroup: FormGroup;
  public activeAccountId: string = null;

  private company_id: string | null = null;

  constructor(
        public _formBuilder: FormBuilder,
        private  _authService: AuthService,
        private _userService: UserService,
        private _router: Router,
        private _route: ActivatedRoute,
        private _sharedService: SharedService
  ) { }

  ngOnInit(): void {
    this.initFormGroup();
    this.subscribeToQueryParam();
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  subscribeToQueryParam() {
    this._route.queryParams.pipe(takeUntil(this._unsubscribeAll)).subscribe(
      (params: any) => {
        this.company_id = params.companyId ? params.companyId : null;
        if(this.company_id){
          this.subscribeToSubject();
        }
      }
    );
  }

  subscribeToSubject() {
    this._userService.company_details.pipe(takeUntil(this._unsubscribeAll)).subscribe(
      (data: CompanyDetails) => {

        if(data.companyId) {
            this.companyObject = data;
            this.patchValue();
            this.is_editing = this.companyObject.companyId != null;
        } else {
            this.loadCompany();
        }


      }
    );
  }

  patchValue() {
    this.companyFormGroup.patchValue ({
      companyAddress: this.companyObject.companyAddress,
      companyName: this.companyObject.companyName,
      companyEmail: this.companyObject.companyEmail,
      companyPhoneNumber: this.companyObject.companyPhoneNumber,
      companyLogo: this.companyObject.companyLogo
    });
  }


  loadCompany():void {
    this.activeAccountId = this._authService.currentActiveAccount.account.accountId;
    console.log("accID", this.activeAccountId);

    this._userService.getCompanyDetails(this.activeAccountId).pipe(takeUntil(this._unsubscribeAll)).subscribe(
      (data: CompanyDetails)  => {
         this.companyObject = data;


         this.patchValue();
         this.is_editing = this.companyObject.companyId != null;
   })

   }

  initFormGroup() {
    return this.companyFormGroup = this._formBuilder.group({

      companyAddress: [this.companyObject.companyAddress, Validators.required],
      companyName: [this.companyObject.companyName, Validators.required],
      companyEmail: [this.companyObject.companyEmail, Validators.required, , Validators.email],
      companyPhoneNumber: [this.companyObject.companyPhoneNumber, Validators.required],
      companyLogo: [this.companyObject.companyLogo, Validators.required],

    })
  }

  updateAccount() {
    if(this.is_editing){
      this.showSpinner = true;

      let company = new UpdateCompany();
      company.companyAddress = this.companyFormGroup.get('companyAddress').value;
      company.companyEmail = this.companyFormGroup.get('companyEmail').value;
      company.companyName = this.companyFormGroup.get('companyName').value;
      company.companyPhoneNumber = this.companyFormGroup.get('companyPhoneNumber').value;
      company.companyLogo = this.companyFormGroup.get('companyLogo').value;
      company.companyId = this.companyObject.companyId;

      this._userService.updateCompany(company).pipe(takeUntil(this._unsubscribeAll)).subscribe(
        (data: CompanyDetails) => {
          if(data) {

              Swal.fire({
                icon: 'success',
                title: 'Profile successfully edited !',
                showConfirmButton: false,
                timer: 2000,
                willClose: () => {
                  this.is_saving_results = false;
                  this.showSpinner = false;
                  this._router.navigate(["user/details/overview"])
                },
              });
          }
        }
      )

    } else {
      let createCompany = new CreateCompany();
        createCompany.companyAddress = this.companyFormGroup.get('companyAddress').value
        createCompany.companyEmail = this.companyFormGroup.get('companyEmail').value
        createCompany.companyName = this.companyFormGroup.get('companyName').value
        createCompany.companyPhoneNumber = this.companyFormGroup.get('companyPhoneNumber').value

        this._userService.createCompany(createCompany).pipe(takeUntil(this._unsubscribeAll)).subscribe(
          (data: CompanyDetails) => {
            if(data) {
              Swal.fire({
                icon: 'success',
                title: 'Profile successfully created !',
                showConfirmButton: false,
                timer: 2000,
                willClose: () => {
                  this.is_saving_results = false;
                  this.showSpinner = false;
                },
              });
            }
          });
          this._router.navigate(["user/details/overview"])
    }
    }
    uploadImage(event: any): void {
      console.log('file --> ', event)
    if (event.target.files.length > 0) {
      this.showSpinner = true;

      const file: File = ((event.target as HTMLInputElement).files as FileList)[0];

      let form_data: FormData = new FormData;
      form_data.append('file', file, file.name)
      console.log('file --> ', file)

      this._userService.mediaImage(form_data).pipe(takeUntil(this._unsubscribeAll)).subscribe(
        (data: string) => {
            Swal.fire({
              icon: 'success',
              text: 'Photo is successfully uploaded!',
              showConfirmButton: false,
              timer: 2000,
              willClose: () => {
                console.log('photo url --> ', data)
                // this.patientForm.get('profilePicture')?.setValue(data);
                this.companyFormGroup.get('companyLogo')?.setValue(data);
                console.log("companyLogo", this.companyFormGroup.get('companyLogo'));
                this.showSpinner = false;
                event.target.value = null;
              }
            });
        },
        () => {
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Something went wrong! Please try again',
              showConfirmButton: false,
              timer: 2000,
              willClose: () => {
                this.showSpinner = false;
                event.target.value = null;
              }
            });
        }
      );

    }
  }


}
