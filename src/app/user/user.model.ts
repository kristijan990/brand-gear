export class CreateCompany{
  constructor(
    public companyName: string | null = null,
    public companyEmail: string | null = null,
    public companyAddress: string | null = null,
    public companyPhoneNumber: string | null = null

  ) {}
}
export class UpdateCompany{
  constructor(
    public companyName: string | null = null,
    public companyEmail: string | null = null,
    public companyAddress: string | null = null,
    public companyPhoneNumber: string | null = null,
    public companyId: string | null = null,
    public companyLogo: string | null = null

  ) {}
}

export class CompanyDetails{
  constructor(
    public companyName: string | null = null,
    public companyEmail: string | null = null,
    public companyAddress: string | null = null,
    public companyPhoneNumber: string | null = null,
    public companyLogo: string | null = null,
    public companyGuid: string | null = null,
    public companyId: string | null = null,
    public companyCampaigns: Array<CompanyCampaigns> = new Array<CompanyCampaigns>(),

  ) {}
}
export class CompanyCampaigns{
  constructor(
    public campaignId: string | null = null,
    public campaignName: string | null = null,
    public campaignStatus: string | null = null,
    public campaignStatusName: string | null = null,
    public campaignMerch: string | null = null,
    public campaignMerchName: string | null = null,
    public campaignPrice: string | null = null,
    public campaignTerms: string | null = null,
    public campaignPicture: string | null = null,
    public createdBy: string | null = null,
    public createdByName: string | null = null,
    public companyGuid: string | null = null,
    public companyId: string | null = null,
    public companyName: string | null = null,
    public dateCreated: string | null = null,
    public startDate: string | null = null,
    public endDate: string | null = null,
    public isActive: boolean | null = null,
    public campaignGuid: string | null = null,
    // public campaignApplications:

  ) {}
}
export class CampaignApplications<T>{
  constructor(
    public campaignApplicationId: string | null = null,
    public dateCreated: string | null = null,
    public campaignApplicationStatus: string | null = null,
    public campaignApplicationStatusName: string | null = null,
    public applicant: T

  ) {}
}
export class CustomerDetailsObject{
  constructor(
    public customerDetailsId: string | null = null,
    public accountId: string | null = null,
    public customerFirstName: string | null = null,
    public customerLastName: string | null = null,
    public customerPhoneNumber: string | null = null,
    public customerEmail: string | null = null,
    public customerGender: string | null = null,
    public customerGenderName: string | null = null,
    public customerGuid: string | null = null,
    public customerProfilePicture: string | null = null,
    public customerLocation: string | null = null,
    public campaignApplications: Boolean | null = null,
  ) {}
}

export class CustomerCreateDetails{
  constructor(
   public profilePicture: string | null = null,
    public location: string | null = null,
    public accountId: string | null = null,

  ) {}
}

export class CustomerUpdateDetails extends CustomerCreateDetails{
  constructor(
   public customerDetailsId: string | null = null,


  ) {
    super()
  }
}

export class UpdateAccountObj {
  constructor (
    public accountId: string | null = null,
    public firstName: string | null = null,
    public lastName: string | null = null,
    public email: string | null = null,
    public password: string | null = null,
    public accountType: string | null = null,
    public phoneNumber: string | null = null,
    public gender: string | null = null,
    public profilePicture: string | null = null,
  ) {}
}

export class AccountDetailsObject {
  constructor (
    public accountId: string | null = null,
    public profilePicture: string | null = null,
    public location: string | null = null,

  ) {}
}



