import { Injectable } from '@angular/core';
import { BehaviorSubject, map, Observable, catchError,throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AccountObject } from '../auth/auth.model';
import { AuthService } from '../auth/auth.service';
import { ApiResult } from '../shared/shared.model';
import { CompanyDetails, CreateCompany, UpdateAccountObj, UpdateCompany } from './user.model';
import { CustomerCreateDetails, CustomerDetailsObject, CustomerUpdateDetails,  } from './user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public company_details: BehaviorSubject<CompanyDetails> = new BehaviorSubject<CompanyDetails>(new CompanyDetails());
  public customer_details: BehaviorSubject<CustomerDetailsObject> = new BehaviorSubject<CustomerDetailsObject>(new CustomerDetailsObject());

  private _https_header: HttpHeaders = this._authService.tokenHeader();


  constructor(
        private _httpClient: HttpClient,
        private _authService: AuthService,

  ) { }

  public getCompanyDetails(accountId: string): Observable<CompanyDetails> {
    return this._httpClient.get<ApiResult<CompanyDetails>>(`${environment.apiUrl}company/details/by/account?accountId=${encodeURIComponent(accountId)}`, {headers : this._https_header}).pipe(
      map((data: ApiResult<CompanyDetails>) => data.response)
    );

  }

  public getCompany(companyId: string): Observable<CompanyDetails> {
    return this._httpClient.get<ApiResult<CompanyDetails>>(`${environment.apiUrl}company/details?companyId=${encodeURIComponent(companyId)}`, {headers : this._https_header}).pipe(
      map((data: ApiResult<CompanyDetails>) => data.response)
    );

  }

  public getCustomerDetails(guid: string): Observable<CustomerDetailsObject> {
    return this._httpClient.get<ApiResult<CustomerDetailsObject>>(`${environment.apiUrl}customer/details/by/account/guid?accountGuid=${encodeURIComponent(guid)}`, {headers : this._https_header}).pipe(
      map((data: ApiResult<CustomerDetailsObject>) => data.response)
    );

  }

  public createCompany(company: CreateCompany): Observable<CompanyDetails> {
    return this._httpClient.post<ApiResult<CompanyDetails>>(`${environment.apiUrl}company/create`, company, {headers : this._https_header} ).pipe(
      map((data: ApiResult<CompanyDetails>) => data.response)
    );
  }
  public updateCompany(company: UpdateCompany): Observable<CompanyDetails> {
    return this._httpClient.post<ApiResult<CompanyDetails>>(`${environment.apiUrl}company/update`, company, {headers : this._https_header} ).pipe(
      map((data: ApiResult<CompanyDetails>) => data.response)
    );
  }

  public createCustomer(customerCreateDetails: CustomerCreateDetails): Observable<CustomerDetailsObject> {
    return this._httpClient.post<ApiResult<CustomerDetailsObject>>(`${environment.apiUrl}customer/create`, customerCreateDetails, {headers : this._https_header} ).pipe(
      map((data: ApiResult<CustomerDetailsObject>) => data.response)
    );
  }

  public updateCustomer(customerUpdateDetails: CustomerUpdateDetails): Observable<CustomerDetailsObject> {
    return this._httpClient.post<ApiResult<CustomerDetailsObject>>(`${environment.apiUrl}customer/update`, customerUpdateDetails, {headers : this._https_header} ).pipe(
      map((data: ApiResult<CustomerDetailsObject>) => data.response)
    );
  }

  public updateAccount(customerUpdateDetails: UpdateAccountObj): Observable<AccountObject> {
    return this._httpClient.post<ApiResult<AccountObject>>(`${environment.apiUrl}account/update`, customerUpdateDetails, {headers : this._https_header} ).pipe(
      map((data: ApiResult<AccountObject>) => data.response)
    );
  }





 /* Function for handling errors
  *
  * @param { HttpErrorResponse } error
  *
  */
 private _handleError(error: HttpErrorResponse) {
   if (error.error instanceof ErrorEvent) {
     // A client-side or network error occurred. Handle it accordingly.
     console.error('An error occurred:', error.error.message);
   } else {
     // The backend returned an unsuccessful response code. The response body may contain clues as to what went wrong,
     console.error(
       `Backend returned code ${error.status}, ` + `body was: ${error.error}`
     );
   }
   // return an observable with a user-facing error message
   return throwError(error)
 }
  /**
	  * Function to Upload Image and get URL
	  *
	  *  @param { MediaModel } form_data
	  *
	  */
	public mediaImage(form_data: FormData): Observable<string> {
		return this._httpClient.post<ApiResult<string>>(`${environment.apiUrl}media/image/upload`, form_data).pipe(
			map((data: ApiResult<string>) => data.response),
			catchError(this._handleError)
		);
	}


	/**
    * Function to Upload Video and get URL
	  *
	  * @param { FormData } form_data
	  *
    */
	public mediaVideo(form_data: FormData): Observable<any> {
		return this._httpClient.post<ApiResult<string>>(`${environment.apiUrl}media/video/upload`, form_data).pipe(
			map((data: ApiResult<string>) => data.response),
			catchError(this._handleError)
		);
	}


	/**
    * Function to Upload Document and get URL
	  *
	  *  @param { FormData } form_data
	  *
    */
  public mediaDocument(form_data: FormData): Observable<any> {
		return this._httpClient.post<ApiResult<string>>(`${environment.apiUrl}media/document/upload`, form_data).pipe(
		  map((data: ApiResult<string>) => data.response),
		  catchError(this._handleError)
		);
	}
}
