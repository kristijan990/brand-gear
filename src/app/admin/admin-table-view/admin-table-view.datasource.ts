import { DataSource } from "@angular/cdk/collections";
import { BehaviorSubject, Observable, Subject, takeUntil } from "rxjs";
import { AdminCompanyContacts, AdminCompanyDetails, AdminListResponse, CompanyListFilter } from "../admin.model";
import { AdminService } from "../admin.service";


export class AdminDataSource implements DataSource<AdminCompanyDetails[]> {

  dataTable = new BehaviorSubject<AdminCompanyDetails[]>([]);
  totalCount= new BehaviorSubject<number>(0);
  loading = new BehaviorSubject<boolean>(true);
  private _unsubscribeAll: Subject<void> = new Subject<void>();

  constructor(
    private _adminService: AdminService

  ) { }

  getApiData(filter: CompanyListFilter) {
    this.loading.next(true);

    this._adminService.getCompanyList(filter).pipe().subscribe(
      (data: AdminListResponse<Array<AdminCompanyDetails>>) => {

        console.log('table data -> ',data)
        if(typeof data != 'string'){
          this.dataTable.next(data.data);
          this.totalCount.next(data.totalRecords);

        }
        this.loading.next(false);

      }

    )
}

  connect(): Observable<any> {
      return this.dataTable;
  }

  disconnect(): void {
      this.dataTable.complete();
  }

}
