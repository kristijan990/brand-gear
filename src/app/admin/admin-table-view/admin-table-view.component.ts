import { trigger, state, style, transition, animate } from '@angular/animations';
import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, Subject, takeUntil } from 'rxjs';
import { AdminCompanyDetails, CompanyListFilter } from '../admin.model';
import { AdminService } from '../admin.service';
import { AdminDataSource } from './admin-table-view.datasource';

@Component({
  selector: 'app-admin-table-view',
  templateUrl: './admin-table-view.component.html',
  styleUrls: ['./admin-table-view.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class AdminTableViewComponent implements OnInit, OnDestroy, AfterViewInit{

  _unsubscribeAll: Subject<void> = new Subject();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  displayedColumns = ['companyLogo', 'companyName', 'companyAddress', 'companyPhoneNumber', 'companyEmail', 'contactInformation'];

  expandedElement: string = 'collapsed';
  companyElement: AdminCompanyDetails | null = null;

  dataSource: AdminDataSource;
  totalCount: number = 0;
  show_table: boolean = false;
  show_spinner: boolean = true;
  filter: CompanyListFilter = new CompanyListFilter();

  constructor(
    private _adminService: AdminService
  ) { }

  ngOnInit(): void {

      this.dataSource = new AdminDataSource(this._adminService);

      this.filter.pageNumber = 1;
      this.filter.pageSize = 10;
      this.filter.sortColumn = '';
      this.filter.sortDirection = '';

      this.dataSource.getApiData(this.filter);

      // Recieve api data
      this.dataSource.dataTable.pipe(takeUntil(this._unsubscribeAll)).subscribe(
        (data: AdminCompanyDetails[]) => {
        console.log("aplicant data -->", data)
         if (data.length > 0) {
            this.show_table = true;

          }
        }
      )

      // Recieve total items ( number of rows )
      this.dataSource.totalCount.pipe(takeUntil(this._unsubscribeAll)).subscribe(
        (data: number) => {
          this.totalCount = data;
        }
      );
      // Show/hide spinner
      this.dataSource.loading.pipe(takeUntil(this._unsubscribeAll)).subscribe(
        (data: boolean) => {
          this.show_spinner = data;
        }
      );

  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
}

ngAfterViewInit(): void {
    merge(this.sort.sortChange, this.paginator.page).pipe(takeUntil(this._unsubscribeAll)).subscribe(
      data => {
        this.reLoadTable();
      }
    )
}

reLoadTable() {
  this.filter.pageNumber = this.paginator.pageIndex + 1;
  this.filter.pageSize = this.paginator.pageSize;
  this.filter.sortColumn = this.sort.active;
  this.filter.sortDirection = this.sort.direction;

  this.dataSource.getApiData(this.filter);
}


expandRow(row: AdminCompanyDetails) {
  this.companyElement = this.companyElement == row ? null : row;
  console.log("row", row)
}
}
