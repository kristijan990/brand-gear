import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthService } from '../auth/auth.service';
import { ApiResult } from '../shared/shared.model';
import { AdminCompanyDetails, AdminListResponse, CompanyListFilter } from './admin.model';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  private _https_header: HttpHeaders = this._authService.tokenHeader();
  token = this._cookieService.get('BranGearToken');
  constructor(
      private _httpClient: HttpClient,
      private _cookieService: CookieService,
      private _authService: AuthService

      ) { }

  public getCompanyList(filter: CompanyListFilter): Observable<AdminListResponse<Array<AdminCompanyDetails> > > {
    return this._httpClient
      .post<ApiResult<AdminListResponse<Array<AdminCompanyDetails>>>>(`${environment.apiUrl}company/list`, filter, {
        headers: this._https_header,
      })
      .pipe(
        map(
          (
            data: ApiResult<
            AdminListResponse<Array<AdminCompanyDetails> >
            >
          ) => data.response
        )
      );
  }
}
