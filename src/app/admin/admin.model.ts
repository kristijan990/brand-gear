export class AdminCompanyDetails {
  constructor(
    public companyId: string | null = null,
    public companyName: string | null = null,
    public companyEmail: string | null = null,
    public companyAddress: string | null = null,
    public companyPhoneNumber: string | null = null,
    public companyGuid: string | null = null,
    public companyLogo: string | null = null,
    public contactInformation: AdminCompanyContacts | null = null

  ) {}
}

export class AdminCompanyContacts {
  constructor(
    public contactFirstName: string | null = null,
    public contactLastName: string | null = null,
    public contactEmail: string | null = null,
    public contactPhoneNumber: string | null = null,

  ) {}
}

export class CompanyListFilter {
  constructor(
    public pageNumber: number | null = null,
    public pageSize: number | null = null,
    public sortColumn: string | null = null,
    public sortDirection: string | null = null,

  ) {}
}

export class AdminListResponse<T> {
  constructor(
    public pageNumber: number | null = null,
    public pageSize: number | null = null,
    public totalPages: number | null = null,
    public totalRecords: number | null = null,
    public data: T
  ) {}
}
