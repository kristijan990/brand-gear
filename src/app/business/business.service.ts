import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthService } from '../auth/auth.service';
import { ApiResult } from '../shared/shared.model';
import { CompanyDetails } from '../user/user.model';

@Injectable({
  providedIn: 'root'
})
export class BusinessService {
  private _https_header: HttpHeaders = this._authService.tokenHeader();
  token = this._cookieService.get('BranGearToken');



  constructor(
              private _httpClient: HttpClient,
              private _authService: AuthService,
              private _cookieService: CookieService,

  ) { }

  getCompanyByGuid(companyGuid : string): Observable<CompanyDetails> {
    return this._httpClient.get<ApiResult<CompanyDetails>>(`${environment.apiUrl}company/details/by/guid?companyGuid=${encodeURIComponent(companyGuid )}  `, { headers: this._https_header }).pipe(
      map((data: ApiResult<CompanyDetails>) => data.response)

    );
  }


}
