import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusinessComponent } from './business.component';
import { BusinessRoutingModule } from './business-routing.module';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { DetailsCompanyComponent } from './details-company/details-company.component';
import { CompanyAplicationsTableViewComponent } from './details-company/company-aplications-table-view/company-aplications-table-view.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import {MatTooltipModule} from '@angular/material/tooltip';




@NgModule({
  declarations: [
    BusinessComponent,
    DetailsCompanyComponent,
    CompanyAplicationsTableViewComponent,



  ],
  imports: [
    CommonModule,
    BusinessRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    MatTooltipModule
  ],
  exports: [
    CompanyAplicationsTableViewComponent
  ]
})
export class BusinessModule { }
