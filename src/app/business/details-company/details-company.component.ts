import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { CompanyDetails } from 'src/app/user/user.model';
import { BusinessService } from '../business.service';

@Component({
  selector: 'app-details-company',
  templateUrl: './details-company.component.html',
  styleUrls: ['./details-company.component.scss']
})
export class DetailsCompanyComponent implements OnInit {

  private _unsubscribeAll: Subject<void> = new Subject<void>();
  companyDetails: CompanyDetails = new CompanyDetails();
  activeGuid: string = null;
  showSpinner: Boolean = false;
  companyId: string = null;

  constructor(
               private _router: Router,
               private _activatedRoute: ActivatedRoute,
               private _bussinesService: BusinessService
  ) { }

  ngOnInit(): void {
    this._activatedRoute.params.pipe(takeUntil(this._unsubscribeAll)).subscribe(
      (params: Params) => {
        if(params["guid"]){
          this.activeGuid = params["guid"];
          console.log("activeGuid", this.activeGuid);

         this.showSpinner = true;


        }
      }

    )
    this.getCompanyDetails();
  }

 getCompanyDetails() {
   this._bussinesService.getCompanyByGuid(this.activeGuid).pipe(takeUntil(this._unsubscribeAll)).subscribe(
     (data: CompanyDetails) => {
      this.companyDetails = data;
      this.companyId = data.companyId;
      console.log("Company Details --->", this.companyDetails)
      this.showSpinner = false;
     }

   )

 }

}
