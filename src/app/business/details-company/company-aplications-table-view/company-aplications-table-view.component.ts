import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { TooltipPosition } from '@angular/material/tooltip';
import { ActivatedRoute, Router } from '@angular/router';
import { merge, Subject, takeUntil } from 'rxjs';
import { CampaignsDetails } from 'src/app/campaign/campaign.model';
import { CampaignService } from 'src/app/campaign/campaign.service';
import { DataTableFilter } from 'src/app/shared/shared.model';
import { BusinessService } from '../../business.service';
import { CompanyApplicationsDataSource } from './company-aplications-table.datasource';

@Component({
  selector: 'app-company-aplications-table-view',
  templateUrl: './company-aplications-table-view.component.html',
  styleUrls: ['./company-aplications-table-view.component.scss']
})
export class CompanyAplicationsTableViewComponent implements OnInit {

  _unsubscribeAll: Subject<void> = new Subject();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  displayedColumns = ['campaignPicture', 'campaignName', 'campaignMerchName', 'campaignPrice', 'campaignStatusName', 'buttons'];

  dataSource: CompanyApplicationsDataSource;
  filter: DataTableFilter = new DataTableFilter();
  totalCount: number = 0;
  show_spinner: boolean = true;
  show_table: boolean = false;

   campaign: CampaignsDetails = new CampaignsDetails();
   @Input() companyId: string;

  constructor(
              private _campaignService: CampaignService,
              private _businessService: BusinessService,
              private _router: Router,
              private _route: ActivatedRoute

  ) {

  }


  ngOnInit(): void {

    if(this.companyId){
      this.dataSource = new CompanyApplicationsDataSource(this._campaignService);

      this.filter.pageNumber = 1;
      this.filter.pageSize = 5;
      this.filter.sortColumn = '';
      this.filter.sortDirection = '';
      this.filter.companyId = this.companyId;

      this.dataSource.getApiData(this.filter);

      // Recieve api data
      this.dataSource.dataTable.pipe(takeUntil(this._unsubscribeAll)).subscribe(
        (data: CampaignsDetails[]) => {
        console.log("aplicant data -->", data)
         if (data.length > 0) {
            this.show_table = true;

          }


        }
      )

      // Recieve total items ( number of rows )
      this.dataSource.totalCount.pipe(takeUntil(this._unsubscribeAll)).subscribe(
        (data: number) => {
          this.totalCount = data;
        }
      );
      // Show/hide spinner
      this.dataSource.loading.pipe(takeUntil(this._unsubscribeAll)).subscribe(
        (data: boolean) => {
          this.show_spinner = data;
        }
      );
    }

  }






  ngOnDestroy(): void {
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
  }

  ngAfterViewInit(): void {
      merge(this.sort.sortChange, this.paginator.page).pipe(takeUntil(this._unsubscribeAll)).subscribe(
        data => {
          this.reLoadTable();
        }
      )
  }




  reLoadTable() {
    this.filter.pageNumber = this.paginator.pageIndex + 1;
    this.filter.pageSize = this.paginator.pageSize;
    this.filter.sortColumn = this.sort.active;
    this.filter.sortDirection = this.sort.direction;

    this.dataSource.getApiData(this.filter);
  }

  viewCampaign(row): void {
    this._router.navigate([`../../../campaign/details/${row.campaignGuid}`], {relativeTo: this._route })

    console.log("guid" , row.campaignGuid)

  }

}
