import { DataSource } from "@angular/cdk/collections";
import { OnDestroy } from "@angular/core";
import { ActivatedRoute, Params } from "@angular/router";
import { BehaviorSubject, Observable, Subject, takeUntil } from "rxjs";
import { CampaignCustomerListResponce, CampaignsAplicantObject, CampaignsDetails, FilterDataTable } from "src/app/campaign/campaign.model";
import { CampaignService } from "src/app/campaign/campaign.service";
import { DataTableFilter, DataTableResponse } from "src/app/shared/shared.model";




export class CompanyApplicationsDataSource implements DataSource<[]>  {

  dataTable = new BehaviorSubject<CampaignsDetails[]>([]);

  totalCount= new BehaviorSubject<number>(0);
  loading = new BehaviorSubject<boolean>(true);
  activeGuid: string = null;
  private _unsubscribeAll: Subject<void> = new Subject<void>();

  constructor(
               private _campaignService: CampaignService,


  ) {

  }



  getApiData(filter: DataTableFilter) {
    this.loading.next(true);

    this._campaignService.listCampaign(filter).pipe(takeUntil(this._unsubscribeAll)).subscribe(
      (data: DataTableResponse<CampaignsDetails[]> | string) => {

        console.log('table data -> ',data)
        if(typeof data != 'string'){
          this.dataTable.next(data.data);

          console.log('data.data', data.data)

          this.totalCount.next(data.totalRecords);


        }
        this.loading.next(false);


      }

    )

  }

  connect(): Observable<any> {
      return this.dataTable;
  }

  disconnect(): void {
      this.dataTable.complete();
  }

}
