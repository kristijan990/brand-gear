import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BusinessComponent } from './business.component';
import { DetailsCompanyComponent } from './details-company/details-company.component';


const businessRoutes: Routes = [
  {
    path: '',
    component: BusinessComponent,
    children: [
      {
        path: 'details/:guid',
        component: DetailsCompanyComponent
      }
    ]

  }
];

@NgModule({
  imports: [RouterModule.forChild(businessRoutes)],
  exports: [RouterModule],
})
export class BusinessRoutingModule {}
