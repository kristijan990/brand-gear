import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { CustomerObject } from '../customer.model';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-customer-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.scss']
})
export class CustomerFormComponent implements OnInit, OnDestroy {

  is_editing: Boolean;

  customerObject: CustomerObject = new CustomerObject();
  customerFormGroup: FormGroup;
  private _unsubscribeAll: Subject<void> = new Subject<void>();

  constructor(
        public _formBuilder: FormBuilder,
        private _customerService: CustomerService,
        private _router: Router
  ) {

  }

  ngOnInit(): void {


    this.initFormGroup();
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  initFormGroup() {

    return this.customerFormGroup = this._formBuilder.group({
      profilePicture: [this.customerObject.profilePicture, Validators.required],
      location: [this.customerObject.location, Validators.required],

    })
  }
  updateAccount() {

    if(this.is_editing){
      let customer = new CustomerObject();
      customer.location = this.customerFormGroup.get('location').value;
      customer.profilePicture = this.customerFormGroup.get('profilePicture').value;

      this._customerService.updateCustomer(customer).pipe(takeUntil(this._unsubscribeAll)).subscribe(
        (data: CustomerObject) => {
          if(data) {
          }
        }
      )
    } else {
        let createCustomer = new CustomerObject();
        createCustomer.location = this.customerFormGroup.get('location').value
        createCustomer.profilePicture = this.customerFormGroup.get('profilePicture').value

        this._customerService.createCustomer(createCustomer).pipe(takeUntil(this._unsubscribeAll)).subscribe(
          (data: CustomerObject) => {

          }
        )
      }


  }


}
