import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthService } from '../auth/auth.service';
import {
  CampaignCustomerListResponce,
  CampaignsDetails,
} from '../campaign/campaign.model';
import {
  ApiResult,
  CreateRating,
  DataTableResponse,
  FilterRatingList,
  RatingDetails,
  RatingObject,
} from '../shared/shared.model';
import {
  AplicantObject,
  CustomerFilterData,
  CustomerObject,
} from './customer.model';

@Injectable({
  providedIn: 'root',
})
export class CustomerService {
  private _https_header: HttpHeaders = this._authService.tokenHeader();

  constructor(
    private _httpClient: HttpClient,
    private _authService: AuthService,
    private _cookieService: CookieService
  ) {}

  public createCustomer(customer: CustomerObject): Observable<CustomerObject> {
    return this._httpClient
      .post<ApiResult<CustomerObject>>(
        `${environment.apiUrl}details/create`,
        customer
      )
      .pipe(map((data: ApiResult<CustomerObject>) => data.response));
  }
  public updateCustomer(customer: CustomerObject): Observable<CustomerObject> {
    return this._httpClient
      .post<ApiResult<CustomerObject>>(
        `${environment.apiUrl}details/update`,
        customer
      )
      .pipe(map((data: ApiResult<CustomerObject>) => data.response));
  }
  public getCustomerGuid(customerGuid: string): Observable<AplicantObject> {
    return this._httpClient
      .get<ApiResult<AplicantObject>>(
        `${
          environment.apiUrl
        }customer/details/by/account/guid?accountGuid=${encodeURIComponent(
          customerGuid
        )}`,
        { headers: this._https_header }
      )
      .pipe(map((data: ApiResult<AplicantObject>) => data.response));
  }
  public getCustomerCampaignList(
    filter: CustomerFilterData
  ): Observable<DataTableResponse<Array<CampaignsDetails>>> {
    return this._httpClient
      .post<ApiResult<DataTableResponse<Array<CampaignsDetails>>>>(
        `${environment.apiUrl}campaign/customer/list`,
        filter,
        {
          headers: this._https_header,
        }
      )
      .pipe(
        map(
          (data: ApiResult<DataTableResponse<Array<CampaignsDetails>>>) =>
            data.response
        )
      );
  }

  deleteRating(feedbackId: string): Observable<Boolean> {
    return this._httpClient
      .get<ApiResult<Boolean>>(
        `${environment.apiUrl}feedback/remove?feedbackId=${encodeURIComponent(
          feedbackId
        )}  `,
        { headers: this._https_header }
      )
      .pipe(map((data: ApiResult<Boolean>) => data.response));
  }
}
