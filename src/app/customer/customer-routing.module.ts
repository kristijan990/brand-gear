import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomerDetailsComponent } from './customer-details/customer-details.component';
import { CustomerComponent } from './customer.component';

const customerRoutes: Routes = [
  {
    path: '',
    component: CustomerComponent,
    children: [
      {
        path: 'details/:guid',
        component: CustomerDetailsComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(customerRoutes)],
  exports: [RouterModule]
})
export class CustomerRoutingModule { }
