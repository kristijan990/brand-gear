import { DataSource } from "@angular/cdk/collections";
import { BehaviorSubject, Observable } from "rxjs";
import { CampaignCustomerListResponce, CampaignsAplicantObject, CampaignsAplications, CampaignsDetails, FilterDataTable } from "src/app/campaign/campaign.model";
import { DataTableResponse } from "src/app/shared/shared.model";
import { AplicantObject, CustomerFilterData } from "../../customer.model";
import { CustomerService } from "../../customer.service";

export class CustomerApplicationsDataSource implements DataSource<CampaignsDetails[]> {

  dataTable = new BehaviorSubject<CampaignsDetails[]>([]);
  totalCount= new BehaviorSubject<number>(0);
  loading = new BehaviorSubject<boolean>(true);

  constructor(
      private _customerService: CustomerService,
  ) { }

  getApiData(filter: CustomerFilterData) {
      this.loading.next(true);

      this._customerService.getCustomerCampaignList(filter).pipe().subscribe(
        (data: DataTableResponse<CampaignsDetails[]> | string) => {

          console.log('table data -> ',data)
          if(typeof data != 'string'){
            this.dataTable.next(data.data);
            this.totalCount.next(data.totalRecords);

          }
          this.loading.next(false);

        }

      )
  }

  connect(): Observable<any> {
      return this.dataTable;
  }

  disconnect(): void {
      this.dataTable.complete();
  }

}
