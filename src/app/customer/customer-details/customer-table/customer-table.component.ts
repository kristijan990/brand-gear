import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute, Router } from '@angular/router';
import { merge, Subject, takeUntil } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';

import {
  CampaignsDetails,
  OpenRatingDialogDataObject,
} from 'src/app/campaign/campaign.model';
import { ProofFormComponent } from 'src/app/shared/proof-form/proof-form.component';
import { RatingFormComponent } from 'src/app/shared/rating-form/rating-form.component';
import { FilterRatingList, RatingDetails } from 'src/app/shared/shared.model';
import { SharedService } from 'src/app/shared/shared.service';
import Swal from 'sweetalert2';
import { AplicantObject, CustomerFilterData } from '../../customer.model';
import { CustomerService } from '../../customer.service';
import { CustomerApplicationsDataSource } from './customer-table.datasource';

@Component({
  selector: 'app-customer-table',
  templateUrl: './customer-table.component.html',
  styleUrls: ['./customer-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class CustomerTableComponent implements OnInit {
  @Input() campaignId: string;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  displayedColumns = [
    'campaignPicture',
    'campaignName',
    'merches',
    'startDate',
    'endDate',
    'proof',
    'option',
  ];

  ratingElement: CampaignsDetails | null = null;

  dataSource: CustomerApplicationsDataSource;
  filter: CustomerFilterData = new CustomerFilterData();
  totalCount: number = 0;
  show_spinner: boolean = true;
  show_table: boolean = false;
  has_comment: boolean = true;
  campaignApplicationId: string;
  private _unsubscribeAll: Subject<void> = new Subject<void>();
  aplicantDetails: AplicantObject;
  customerDetails: AplicantObject = new AplicantObject();
  @Input() accountId: string;
  campaign: CampaignsDetails = new CampaignsDetails();
  feedbackId: string = null;
  expandedElement: string = 'collapsed';
  rating: RatingDetails = new RatingDetails();
  is_company: Boolean = false;

  constructor(
    private _customerService: CustomerService,
    private _router: Router,
    private _route: ActivatedRoute,
    public dialog: MatDialog,
    private _authService: AuthService,
    private _sharedService: SharedService
  ) {}

  ngOnInit(): void {
    if (this.accountId) {
      this.dataSource = new CustomerApplicationsDataSource(
        this._customerService
      );

      this.filter.pageNumber = 1;
      this.filter.pageSize = 10;
      this.filter.sortColumn = '';
      this.filter.sortDirection = '';
      this.filter.accountId = this.accountId;

      this.dataSource.getApiData(this.filter);

      // Recieve api data
      this.dataSource.dataTable
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe((data: CampaignsDetails[]) => {
          if (data.length > 0) {
            this.show_table = true;
          }
          if (data) {
          }
        });

      // Recieve total items ( number of rows )
      this.dataSource.totalCount
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe((data: number) => {
          this.totalCount = data;
        });
      // Show/hide spinner
      this.dataSource.loading
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe((data: boolean) => {
          this.show_spinner = data;
        });
    }
    this.checkAccountType();
  }

  checkAccountType() {
    this._sharedService.is_comapny
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((data: Boolean) => {
        this.is_company = data;
      });
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  ngAfterViewInit(): void {
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((data) => {
        this.reLoadTable();
      });
  }

  reLoadTable() {
    this.filter.pageNumber = this.paginator.pageIndex + 1;
    this.filter.pageSize = this.paginator.pageSize;
    this.filter.sortColumn = this.sort.active;
    this.filter.sortDirection = this.sort.direction;

    this.dataSource.getApiData(this.filter);
  }

  viewCampaign(row): void {
    this._router.navigate([`../../../campaign/details/${row.campaignGuid}`], {
      relativeTo: this._route,
    });

    console.log('guid', row.campaignGuid);
  }

  expandRow(row: CampaignsDetails) {
    this.ratingElement = this.ratingElement == row ? null : row;
    console.log('row', row);
  }

  openRatingForm(campaign: CampaignsDetails) {
    let object_to_dialog: OpenRatingDialogDataObject =
      new OpenRatingDialogDataObject();
    object_to_dialog.campaign_id = campaign.campaignId;

    const dialogRef = this.dialog.open(RatingFormComponent, {
      data: object_to_dialog,
      width: '600px',
      height: '400px',
    });
    dialogRef.afterClosed().subscribe((response) => {
      if (response) {
        this.reLoadTable();
      }
    });
  }

  addProof(campaign: CampaignsDetails) {
    const dialogRef = this.dialog.open(ProofFormComponent, {
      width: '600px',

      data: campaign,
    });
    dialogRef.afterClosed().subscribe((response) => {
      if (response) {
        // this.getCampaignList();
      }
    });
  }

  removeRating(feedbackId: string) {
    this._customerService
      .deleteRating(feedbackId)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((data: Boolean) => {
        if (data) {
          Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
          }).then((result) => {
            if (result.isConfirmed) {
              Swal.fire('Deleted!', 'Your file has been deleted.', 'success');
            }
            this.reLoadTable();
          });
        }
      });
  }
}
