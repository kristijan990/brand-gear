import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { CampaignsDetails } from 'src/app/campaign/campaign.model';
import { CampaignService } from 'src/app/campaign/campaign.service';
import {
  DataTableResponse,
  FilterRatingList,
  RatingDetails,
} from 'src/app/shared/shared.model';
import { AplicantObject, CustomerFilterData } from '../customer.model';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.scss'],
})
export class CustomerDetailsComponent implements OnInit, OnDestroy {
  private unsubscribeAll: Subject<void> = new Subject<void>();
  activeCustomerDetails: AplicantObject;
  customerDetails: AplicantObject = new AplicantObject();
  is_loaded: Boolean = false;
  has_aplications: Boolean = false;
  showSpinner: boolean = false;
  filter: CustomerFilterData = new CustomerFilterData();
  accountId: string = null;
  private _unsubscribeAll: Subject<void> = new Subject<void>();
  campaignList: Array<CampaignsDetails> = new Array<CampaignsDetails>();

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _customerService: CustomerService,
    private _campaignService: CampaignService
  ) {}

  ngOnDestroy(): void {
    this.unsubscribeAll.complete();
    this.unsubscribeAll.next();
  }

  ngOnInit(): void {
    this.showSpinner = true;

    this._activatedRoute.params
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe((params: Params) => {
        if (params['guid']) {
          this.getCustomerDetails(params['guid']);
        }
      });
  }

  getCustomerDetails(guid: string): void {
    this._customerService
      .getCustomerGuid(guid)
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe((customerDetails: AplicantObject) => {
        console.log('CUSTOMER DETAILS: ', customerDetails);
        this.activeCustomerDetails = customerDetails;
        this.accountId = customerDetails.accountId;
        this.is_loaded = true;
        if ((this.customerDetails.campaignApplications = true)) {
          this.has_aplications = true;
        } else {
          this.has_aplications = false;
        }
        this.showSpinner = false;
      });
  }

  getCustomerCampaignList() {
    this.filter.accountId = this.accountId;
    this.filter.pageNumber = 1;
    this.filter.sortColumn = '';
    this.filter.sortDirection = '';
    this.filter.pageSize = 5;

    this._customerService
      .getCustomerCampaignList(this.filter)
      .pipe()
      .subscribe((data: DataTableResponse<Array<CampaignsDetails>>) => {
        this.campaignList = data.data;
      });
  }
}
