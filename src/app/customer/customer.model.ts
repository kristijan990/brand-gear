export class CustomerObject{
  constructor(
    public customerDetailsId: string | null = null,
    public accountId: string | null = null,
    public profilePicture: string | null = null,
    public location: string | null = null

  ) {}
}

export class AplicantObject{
  constructor(
    public customerDetailsId: string | null = null,
    public accountId: string | null = null,
    public customerFirstName: string | null = null,
    public customerLastName: string | null = null,
    public customerPhoneNumber: string | null = null,
    public customerEmail: string | null = null,
    public customerGender: string | null = null,
    public customerGenderName: string | null = null,
    public customerGuid: string | null = null,
    public customerProfilePicture: string | null = null,
    public customerLocation: string | null = null,
    public campaignApplications: boolean| null = null,
  ) {}
}

export class CustomerFilterData {
  constructor(
    public pageNumber: number | null = null,
    public pageSize: number | null = null,
    public sortColumn: string | null = null,
    public sortDirection: string | null = null,
    public accountId: string | null = null
  ) {}
}

