import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  CreateRating,
  DataTableFilter,
  DataTableResponse,
  FilterRatingList,
  RatingDetails,
  RatingObject,
} from '../shared/shared.model';
import { BehaviorSubject, map, Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthService } from '../auth/auth.service';
import { ApiResult } from '../shared/shared.model';
import { CookieService } from 'ngx-cookie-service';
import {
  CampaignCustomerListResponce,
  CampaignsAplicantObject,
  CampaignsAplications,
  CampaignsDetails,
  CreateCampaign,
  FilterDataTable,
  LookUpItem,
  ProofDetails,
  tableCustomFilter,
  tableFilter,
  UpdateCampaign,
} from './campaign.model';
import { CompanyDetails } from '../user/user.model';
import { ProofFilter } from '../event/event.model';

@Injectable({
  providedIn: 'root',
})
export class CampaignService {
  private _https_header: HttpHeaders = this._authService.tokenHeader();
  token = this._cookieService.get('BranGearToken');

  constructor(
    private _httpClient: HttpClient,
    private _cookieService: CookieService,
    private _authService: AuthService
  ) {}

  // getCampaign():Observable<

  listCampaign(
    filter: DataTableFilter
  ): Observable<DataTableResponse<Array<CampaignsDetails>>> {
    return this._httpClient
      .post<ApiResult<DataTableResponse<Array<CampaignsDetails>>>>(
        `${environment.apiUrl}campaign/list`,
        filter,
        { headers: this._https_header }
      )
      .pipe(
        map(
          (data: ApiResult<DataTableResponse<Array<CampaignsDetails>>>) =>
            data.response
        )
      );
  }
  showTable: Subject<void> = new Subject();
  applicationsTableData = new BehaviorSubject<CampaignsAplications[]>([]);

  // token: string = 'eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTI1NiIsInR5cCI6IkpXVCJ9.eyJNZW1iZXJJZCI6Iisza1Zpc0ZVazhDeURvSExVMk82OWc9PSIsIkNvbXBhbnlJZCI6Ik5WdmthaVFtY1B6NFJvRndEVDBod0E9PSIsIkh5cGVyQWRtaW5JZCI6Ik4vQSIsIklzU3VwZXJTdXBwbGllciI6IlRydWUiLCJNZW1iZXJUeXBlIjoiZzVtaEROaC9KS2QxM2xzME0zUWhNdz09IiwiRmlyc3ROYW1lIjoiVGhvbWFzIiwiTGFzdE5hbWUiOiJLZWxsZXIiLCJFbWFpbCI6IlZ4UVFNSkQ2cDVXWUtETURQQW5OV0syTFY4NE5PRytPWXpTbm1SajFEZUk9IiwiTGF0aXR1ZGUiOiJOL0EiLCJMb25naXR1ZGUiOiJOL0EiLCJNZW1iZXJUaW1lWm9uZSI6IkV1cm9wZS9Mb25kb24iLCJDdXJyZW5jeUNvZGUiOiJHQlAiLCJDdXJyZW5jeUlkIjoiWDBUQlVyWEZ6QXpOS3IyWmtUME9xdz09IiwiZXhwIjoxNjQ0NDIyNjc0LCJpc3MiOiJodHRwOi8vZXF1YW5ldC5zaXRlIiwiYXVkIjoiaHR0cDovL2VxdWFuZXQuc2l0ZSJ9.MUVxi9NldSmUaHOLo0Fllbwq7Vsp8vflWASLby4woII';
  // header = new HttpHeaders({
  //   'authtoken' : this.token
  // });

  getCampaignDetails(campaignGuid: string): Observable<CampaignsDetails> {
    return this._httpClient
      .get<ApiResult<CampaignsDetails>>(
        `${
          environment.apiUrl
        }campaign/details/by/guid?campaignGuid=${encodeURIComponent(
          campaignGuid
        )}`,
        { headers: this._https_header }
      )
      .pipe(map((data: ApiResult<CampaignsDetails>) => data.response));
  }

  getCampaignAplicant(
    filter: FilterDataTable
  ): Observable<
    CampaignCustomerListResponce<CampaignsAplicantObject> | string
  > {
    return this._httpClient
      .post<
        ApiResult<
          CampaignCustomerListResponce<CampaignsAplicantObject> | string
        >
      >(`${environment.apiUrl}campaign/application/list`, filter, {
        headers: this._https_header,
      })
      .pipe(
        map(
          (
            data: ApiResult<
              CampaignCustomerListResponce<CampaignsAplicantObject> | string
            >
          ) => data.response
        )
      );
  }

  acceptCampaignApplication(
    campaignApplicationId: string
  ): Observable<Boolean> {
    return this._httpClient
      .get<ApiResult<Boolean>>(
        `${
          environment.apiUrl
        }campaign/application/accept?campaignApplicationId=${encodeURIComponent(
          campaignApplicationId
        )}  `,
        { headers: this._https_header }
      )
      .pipe(map((data: ApiResult<Boolean>) => data.response));
  }

  declinedCampaignApplication(
    campaignApplicationId: string
  ): Observable<Boolean> {
    return this._httpClient
      .get<ApiResult<Boolean>>(
        `${
          environment.apiUrl
        }campaign/application/deny?campaignApplicationId=${encodeURIComponent(
          campaignApplicationId
        )}  `,
        { headers: this._https_header }
      )
      .pipe(map((data: ApiResult<Boolean>) => data.response));
  }

  // getCampaignList(company: CampaignsListObj): Observable<CampaignsDetails> {
  //   return this._httpClient.post<ApiResult<CampaignsDetails>>(`${environment.apiUrl}campaign/list`,  company, {headers : this._https_header}).pipe(
  //     map((data: ApiResult<CampaignsDetails>) => data.response)
  //   )
  // }

  addNewCampaign(campaign: CreateCampaign): Observable<CampaignsDetails> {
    return this._httpClient
      .post<ApiResult<CampaignsDetails>>(
        `${environment.apiUrl}campaign/create`,
        campaign,
        { headers: this._https_header }
      )
      .pipe(map((data: ApiResult<CampaignsDetails>) => data.response));
  }
  // public updateCampaign(campaign: UpdateCampaign): Observable<CampaignsDetails> {
  //   return this._httpClient.post<ApiResult<CampaignsDetails>>(`${environment.apiUrl}company/update`, campaign, {headers : this._https_header} ).pipe(
  //     map((data: ApiResult<CampaignsDetails>) => data.response)
  //   );
  // }
  // public lookUpGender(): Observable<ApiResult<LookUpItem[]>> {

  updateCampaign(updateCampaign: UpdateCampaign): Observable<CampaignsDetails> {
    return this._httpClient
      .post<ApiResult<CampaignsDetails>>(
        `${environment.apiUrl}campaign/update`,
        updateCampaign,
        { headers: this._https_header }
      )
      .pipe(map((data: ApiResult<CampaignsDetails>) => data.response));
  }

  createCampaignApplication(
    campaignId: string
  ): Observable<CampaignsAplications | string> {
    return this._httpClient
      .get<ApiResult<CampaignsAplications>>(
        `${
          environment.apiUrl
        }campaign/application/create?campaignId=${encodeURIComponent(
          campaignId
        )}  `,
        { headers: this._https_header }
      )
      .pipe(map((data: ApiResult<CampaignsAplications>) => data.response));
  }

  ////////////////////////////////////////////
  // getTableData(filter: tableFilter): Observable<any> {
  //   return this._httpClient.post<any>("https://api.equanet.site/explore/get/offers?id", filter).pipe(
  //     map(data => data)
  //   )
  // }

  // getCustomTableData(filter: tableCustomFilter): Observable<any> {
  //   return this._httpClient.post<any>("https://api.equanet.site/virtual/service/supplier/datatable", filter, {headers: this.header}).pipe(
  //     map(data => data)
  //   )
  // }

  // deleteCampaign(campaignId: string): Observable<Boolean> {
  //   return this._httpClient.get<Boolean>(
  //     `${environment.apiUrl}/remove?campaignId=${encodeURIComponent(campaignId )}`, campaign { headers: this._https_header }
  //   )
  //   .pipe(map((data: ApiResult<CampaignsDetails>) => data.response));
  //   );
  // }

  deleteCampaign(campaignId: string): Observable<Boolean> {
    return this._httpClient
      .get<ApiResult<Boolean>>(
        `${environment.apiUrl}campaign/remove?campaignId=${encodeURIComponent(
          campaignId
        )}  `,
        { headers: this._https_header }
      )
      .pipe(map((data: ApiResult<Boolean>) => data.response));
  }

  public lookUpGender(): Observable<ApiResult<LookUpItem[]>> {
    return this._httpClient.get<ApiResult<LookUpItem[]>>(
      `${environment.apiUrl}lookup/merch/type/list`,
      { headers: this._https_header }
    );
  }

  getRatingList(
    filter: FilterRatingList
  ): Observable<DataTableResponse<Array<RatingDetails>>> {
    return this._httpClient
      .post<ApiResult<DataTableResponse<Array<RatingDetails>>>>(
        `${environment.apiUrl}feedback/list`,
        filter,
        { headers: this._https_header }
      )
      .pipe(
        map(
          (data: ApiResult<DataTableResponse<Array<RatingDetails>>>) =>
            data.response
        )
      );
  }

  listProof(filter: ProofFilter): Observable<DataTableResponse<Array<ProofDetails>>> {
    return this._httpClient
      .post<ApiResult<DataTableResponse<Array<ProofDetails>>>>(`${environment.apiUrl}proof/list`, filter, { headers: this._https_header }).pipe(
         map( (data: ApiResult<DataTableResponse<Array<ProofDetails>>>) => data.response)
      );

  }
  createRating(createRating: CreateRating): Observable<RatingObject> {
    return this._httpClient
      .post<ApiResult<RatingObject>>(
        `${environment.apiUrl}feedback/post`,
        createRating,
        { headers: this._https_header }
      )
      .pipe(map((data: ApiResult<RatingObject>) => data.response));
  }

}
