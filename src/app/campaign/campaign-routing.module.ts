import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CampaignFormComponent } from './campaign-form/campaign-form.component';
import { CampaignDetailsComponent } from './campaign-details/campaign-details.component';
import { CampaignComponent } from './campaign.component';
import { CardListComponent } from './card-list/card-list.component';

const campaignsRoutes: Routes = [
  {
    path: '',
    component: CampaignComponent,
    children: [
      {
        path: 'form',
        component: CampaignFormComponent,
      },
      {
        path: 'list',
        component: CardListComponent,
      },
      {
        path: 'details/:guid',
        component: CampaignDetailsComponent
      },
      {
        path: '**',
        redirectTo: 'list'
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(campaignsRoutes)],
  exports: [RouterModule],
})
export class CampaignRoutingModule {}
