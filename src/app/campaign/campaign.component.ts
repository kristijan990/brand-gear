import { Component, OnDestroy, OnInit } from '@angular/core';
import { SharedService } from '../shared/shared.service';
import { MatDialog } from '@angular/material/dialog';
import { CampaignFormComponent } from './campaign-form/campaign-form.component';
import { Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'app-campaign',
  templateUrl: './campaign.component.html',
  styleUrls: ['./campaign.component.scss'],
})
export class CampaignComponent implements OnInit, OnDestroy {
  isLogin: Boolean = true;
  private _unsubscribeAll: Subject<void> = new Subject<void>();

  constructor(
    private _sharedService: SharedService,
    private dialog: MatDialog

  ) {}

  ngOnInit(): void {
    this.getHeader();
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  addCampaign() {
    const dialogRef = this.dialog.open(CampaignFormComponent, {
      width: '600px',
      height: '600px',
      data : null
    });
    dialogRef.afterClosed().subscribe((response) => {
      if (response) {
      }
    });
  }

  public getHeader() {
    this._sharedService.is_login.pipe(takeUntil(this._unsubscribeAll)).subscribe((data) => {
      data = this.isLogin;
    });
  }
}
