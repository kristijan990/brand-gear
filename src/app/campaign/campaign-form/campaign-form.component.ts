import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { single, Subject, takeUntil } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { ApiResult } from 'src/app/shared/shared.model';
import { CompanyDetails } from 'src/app/user/user.model';
import { UserService } from 'src/app/user/user.service';
import Swal from 'sweetalert2';
import {
  CampaignsDetails,
  CreateCampaign,
  LookUpItem,
  MerchItems,
  UpdateCampaign,
} from '../campaign.model';
import { CampaignService } from '../campaign.service';

@Component({
  selector: 'app-campaign-form',
  templateUrl: './campaign-form.component.html',
  styleUrls: ['./campaign-form.component.scss'],
})
export class CampaignFormComponent implements OnInit {
  private _unsubscribeAll: Subject<void> = new Subject<void>();
  itemType: LookUpItem[] = new Array<LookUpItem>();
  campaignObject: CampaignsDetails = new CampaignsDetails();
  campaignFormGroup!: FormGroup;
  is_editing: boolean = false;
  showSpinner: boolean = false;
  is_saving_results: boolean = false;
  companyObject: CompanyDetails = new CompanyDetails();
  activeAccountId: string = null;
  has_picture: Boolean = false;

  selectedItemTypes: Array<string> = new Array<string>();

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any,
    public _dialogRef: MatDialogRef<CampaignFormComponent>,
    private _campaignService: CampaignService,
    public _formBuilder: FormBuilder,
    private _userService: UserService,
    private _authServis: AuthService
  ) {
    this.campaignFormGroup = this.initContactFormGroup();

    if(!this.is_editing) {
      this.addMerch(null);
    }


    this.campaignObject = this.data ? this.data : new CampaignsDetails();
    console.log('campaign object -> ', this.campaignObject);
    if (this.campaignObject && this.campaignObject.campaignId) {
      this.patchValue();
    }
  }

  get merching(): FormArray {
    return this.campaignFormGroup.get('merching') as FormArray;
  }

  ngOnInit(): void {
    this.loadCompany();
    this.is_editing = false;
    if (this.campaignObject.companyId != null) {
      this.is_editing = true;
    } else {
      this.is_editing = false;
    }
    this.getItemType();
    if (this.is_editing) {
      this.has_picture = false;
    } else {
      this.has_picture = true;
    }
  }

  addMerch(merch_received: MerchItems | null) {
    const merch = merch_received != null ? merch_received : new MerchItems();
    console.log('merch item -> ', merch);

    const merchForm: FormGroup = this._formBuilder.group({
      merch: [merch.merch, Validators.required],
      price: [merch.price, Validators.required],
      campaignId: [merch.campaignId, Validators.required],
      campaignMerchId: [merch.campaignId, Validators.required],
    });

    this.merching.push(merchForm);
    this.selectedItemTypes.push(merch.merch);
  }

  removeMerch(index: number) {
    this.merching.removeAt(index);
    this.selectedItemTypes[index] = null;
  }

  public getItemType() {
    this._campaignService
      .lookUpGender()
      .subscribe((value: ApiResult<LookUpItem[]>) => {
        this.itemType = value.response;
      });
  }

  initContactFormGroup() {
    return this._formBuilder.group({
      campaignName: [this.campaignObject.campaignName, Validators.required],
      campaignTerms: [this.campaignObject.campaignTerms, Validators.required],
      campaignPicture: [
        this.campaignObject.campaignPicture,
        Validators.required,
      ],
      startDate: [this.campaignObject.startDate, Validators.required],
      endDate: [this.campaignObject.endDate, Validators.required],
      createdBy: [this.campaignObject.createdBy, Validators.required],
      companyId: [this.campaignObject.companyId, Validators.required],
      merching: this._formBuilder.array([]),
    });
  }

  loadCompany(): void {
    this.activeAccountId =
      this._authServis.currentActiveAccount.account.accountId;
    console.log('accID', this.activeAccountId);

    this._userService
      .getCompanyDetails(this.activeAccountId)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((data: CompanyDetails) => {
        this.companyObject = data;
        console.log('company details', this.companyObject);
      });
  }

  patchValue() {
    this.campaignFormGroup.patchValue({
      campaignName: this.campaignObject.campaignName,
      campaignPicture: this.campaignObject.campaignPicture,
      startDate: this.campaignObject.startDate,
      endDate: this.campaignObject.endDate,
      campaignTerms: this.campaignObject.campaignTerms,
    });
    this.campaignObject.merches.forEach((merch_item: MerchItems) => {
      this.addMerch(merch_item);
    });
  }

  uploadImage(event: any): void {
    console.log('file --> ', event);
    if (event.target.files.length > 0) {
      this.showSpinner = true;

      const file: File = (
        (event.target as HTMLInputElement).files as FileList
      )[0];

      let form_data: FormData = new FormData();
      form_data.append('file', file, file.name);
      console.log('file --> ', file);

      this._userService
        .mediaImage(form_data)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(
          (data: string) => {
            Swal.fire({
              icon: 'success',
              text: 'Photo is successfully uploaded!',
              showConfirmButton: false,
              timer: 2000,
              willClose: () => {
                console.log('photo url --> ', data);

                this.campaignFormGroup.get('campaignPicture')?.setValue(data);
                console.log(
                  'campaignPicture',
                  this.campaignFormGroup.get('campaignPicture')
                );

                this.showSpinner = false;
                event.target.value = null;
                this.has_picture = false;
              },
            });
          },
          () => {
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Something went wrong! Please try again',
              showConfirmButton: false,
              timer: 2000,
              willClose: () => {
                this.showSpinner = false;
                event.target.value = null;
              },
            });
          }
        );
    }
  }

  itemSelected(i: number) {
    const merch_object: LookUpItem = this.itemType.find((item: LookUpItem) => this.merching.at(i).get('merch').value == item.value);
    this.selectedItemTypes[i] = merch_object.value;
  }

  updatecampaignForm() {
    this.showSpinner = true;

    if (this.is_editing) {
      let campaign = new UpdateCampaign();
      let newCampaignObject = new CampaignsDetails();

      campaign.campaignName = this.campaignFormGroup.get('campaignName').value;

      campaign.campaignPicture =
        this.campaignFormGroup.get('campaignPicture').value;
      campaign.startDate = this.campaignFormGroup.get('startDate').value;
      campaign.endDate = this.campaignFormGroup.get('endDate').value;
      campaign.campaignTerms =
        this.campaignFormGroup.get('campaignTerms').value;
      campaign.companyId = this.campaignObject.companyId;
      campaign.createdBy = this.campaignObject.createdBy;
      campaign.campaignId = this.campaignObject.campaignId;

      let merches: Array<MerchItems> = new Array<MerchItems>();

      (this.merching.value as Array<any>).forEach((merch: any) => {
        const single_merch: MerchItems = new MerchItems();

        console.log('single_merch in update for loop -> ', single_merch);

        single_merch.campaignMerchId = merch.campaignMerchId;
        single_merch.campaignId = merch.campaignId;
        single_merch.price = merch.price;

        const merch_value: string = merch.merch;
        const merch_object: LookUpItem = this.itemType.find((item: LookUpItem) => merch_value == item.value);

        single_merch.merch = merch_object.value;
        single_merch.merchName = merch_object.name;
        single_merch.merchName = merch_object.name;

        merches.push(single_merch);
      });

      campaign.merches = merches;

      this._campaignService
        .updateCampaign(campaign)
        .pipe()
        .subscribe((data: CampaignsDetails) => {
          if (data) {
            newCampaignObject = data;

            this._dialogRef.close(true);
            this.showSpinner = false;
          }
        });
    } else {
      let createCampaign = new CreateCampaign();
      createCampaign.campaignName =
        this.campaignFormGroup.get('campaignName').value;

      createCampaign.campaignPicture =
        this.campaignFormGroup.get('campaignPicture').value;
      createCampaign.startDate = this.campaignFormGroup.get('startDate').value;
      createCampaign.endDate = this.campaignFormGroup.get('endDate').value;
      createCampaign.campaignTerms =
        this.campaignFormGroup.get('campaignTerms').value;

      createCampaign.companyId = this.companyObject.companyId;
      createCampaign.createdBy = this.activeAccountId;

      let merches: Array<MerchItems> = new Array<MerchItems>();

      (this.merching.value as Array<any>).forEach((merch: any) => {
        const single_merch: MerchItems = new MerchItems();

        single_merch.campaignMerchId = merch.campaignMerchId;
        single_merch.campaignId = merch.campaignId;
        single_merch.price = merch.price;

        const merch_object: LookUpItem = merch.merch;

        single_merch.merch = merch_object.value;
        single_merch.merchName = merch_object.name;

        merches.push(single_merch);
      });

      createCampaign.merches = merches;

      this._campaignService
        .addNewCampaign(createCampaign)
        .pipe()
        .subscribe((data: CampaignsDetails) => {
          if (data) {
            Swal.fire({
              icon: 'success',
              title: 'Campaign Created !',
              showConfirmButton: false,
              timer: 2000,
              willClose: () => {
                this.is_saving_results = false;
                this._dialogRef.close(true);
                this.showSpinner = false;
              },
            });
          }
        });
    }
  }
}
