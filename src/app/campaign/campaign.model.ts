import { AccountObject } from '../auth/auth.model';
import { RatingObject } from '../shared/shared.model';

export class Student {
  constructor(
    public team?: string,
    public first_name?: string,
    public last_name?: string,
    public attend?: string,
    public id?: number
  ) {}
}

export class tableFilter {
  constructor(public page_size?: number, public page_index?: number) {}
}

export class tableData {
  constructor(
    public serviceId?: string,
    public serviceName?: string,
    public serviceImage?: string
  ) {}
}

// custom api table

export class tableCustomRow {
  constructor(
    public offer_id?: string,
    public offer_image?: string,
    public offer_name?: string,
    public offer_price?: number
  ) {}
}

export class tableCustomFilter {
  constructor(
    public pageNumber?: number,
    public pageSize?: number,
    public sortColumn?: string,
    public sortDirection?: string
  ) {}
}

///////////

export class CampaignsDetails {
  constructor(
    public campaignId: string | null = null,
    public campaignName: string | null = null,
    public campaignStatus: string | null = null,
    public campaignStatusName: string | null = null,
    public campaignMerchName: string | null = null,
    public campaignTerms: string | null = null,
    public campaignPicture: string | null = null,
    public createdBy: string | null = null,
    public createdByName: string | null = null,
    public companyGuid: string | null = null,
    public companyId: string | null = null,
    public companyName: string | null = null,
    public dateCreated: string | null = null,
    public startDate: string | null = null,
    public endDate: string | null = null,
    public isActive: boolean | null = null,
    public campaignGuid: string | null = null,
    public campaignApplications: boolean | null = null,
    public feedBack: RatingObject | null = null,
    public campaignApplicationId: string | null = null,
    public merches: Array<MerchItems> = new Array<MerchItems>()

  ) {}
}

export class CampaignsAplications {
  constructor(
    public campaignApplicationId: string | null = null,
    public dateCreated: string | null = null,
    public campaignApplicationStatus: string | null = null,
    public campaignApplicationStatusName: string | null = null,
    public applicant: AccountObject | null = null
  ) {}
}

export class CampaignsAplicantObject {
  constructor(
    public campaignApplicationId: string | null = null,
    public dateCreated: string | null = null,
    public campaignApplicationStatus: string | null = null,
    public campaignApplicationStatusName: string | null = null,
    public applicantId: string | null = null,
    public applicantFirstName: string | null = null,
    public applicantLastName: string | null = null,
    public applicantGender: string | null = null,
    public applicantGenderName: string | null = null,
    public applicantPhoneNumber: string | null = null,
    public applicantEmail: string | null = null,
    public applicantGuid: string | null = null
  ) {}
}

export class CreateCampaign {
  constructor(
    public campaignName: string | null = null,
    public campaignTerms: string | null = null,
    public campaignPicture: string | null = null,
    public createdBy: string | null = null,
    public companyId: string | null = null,
    public startDate: string | null = null,
    public endDate: string | null = null,
    public merches: Array<MerchItems> = new Array<MerchItems>()
  ) {}
}

export class UpdateCampaign extends CreateCampaign {
  constructor(public campaignId: string | null = null) {
    super();
  }
}

export class FilterDataTable {
  constructor(
    public pageNumber: number | null = null,
    public pageSize: number | null = null,
    public sortColumn: string | null = null,
    public sortDirection: string | null = null,
    public campaignId: string | null = null,
    public campaignApplicationStatus: string | string = null
  ) {}
}

export class CampaignCustomerListResponce<T> {
  constructor(
    public pageNumber: number | null = null,
    public pageSize: number | null = null,
    public totalPages: number | null = null,
    public totalRecords: number | null = null,
    public data: Array<T> = new Array<T>()
  ) {}
}

export class LookUpItem {
  constructor(
    public value: string | null = null,
    public name: string | null = null,
    public description: string | null = null
  ) {}
}

export class ProofDetails {
  constructor(
    public proofId: string | null = null,
    public campaignApplicationId: string | null = null,
    public remoteIp: string | null = null,
    public ipAddress: string | null = null,
    public certainty: string | null = null,
    public internet: string | null = null,
    public country: string | null = null,
    public regionLocationCode: string | null = null,
    public region: string | null = null,
    public code: string | null = null,
    public city: string | null = null,
    public cityId: string | null = null,
    public fqcn: string | null = null,
    public latitude: string | null = null,
    public longitude: string | null = null,
    public capital: string | null = null,
    public timezone: string | null = null,
    public nationalitySingular: string | null = null,
    public population: string | null = null,
    public nationalityPlural: string | null = null,
    public mapReference: string | null = null,
    public currency: string | null = null,
    public currencyCode: string | null = null,
    public title: string | null = null,
    public osName: string | null = null,
    public osVersion: string | null = null,
    public browserName: string | null = null,
    public browserVersion: string | null = null,
    public userAgent: string | null = null,
    public appVersion: string | null = null,
    public plarform: string | null = null,
    public vendor: string | null = null,
    public images: ProofImages[] | null = null,
     public videos: ProofVideos[] | null = null,
  ) {}
}

export class ProofImages {
  constructor(
    public proofImageId: string | null = null,
    public proofId: string | null = null,
    public imageUrl: string | null = null
  ) {}
}

export class ProofVideos {
  constructor(
    public proofVideoId: string | null = null,
    public proofId: string | null = null,
    public videoUrl: string | null = null
  ) {}
}
export class MerchItems {
  constructor(
    public campaignId: string | null = null,
    public merch: string | null = null,
    public price: string | null = null,
    public merchName: string | null = null,
    public campaignMerchId: string | null = null,
    ) {}
  }

export class OpenRatingDialogDataObject {
  constructor(
    public campaign_id: string | null = null,
    public applicant: CampaignsAplicantObject = new CampaignsAplicantObject()
  ) {}
}

export class OpenRatingDialogCampaignObject {
  constructor(
    public campaign_id: string | null = null,
    public applicant: CampaignsDetails = new CampaignsDetails()
  ) {}
}
