import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CampaignComponent } from './campaign.component';
import { CampaignFormComponent } from './campaign-form/campaign-form.component';
import { CampaignRoutingModule } from './campaign-routing.module';
import { CampaignCardViewComponent } from './campaign-card-view/campaign-card-view.component';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { CardListComponent } from './card-list/card-list.component';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { MatNativeDateModule } from '@angular/material/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { CampaignDetailsComponent } from './campaign-details/campaign-details.component';
import { CampaignApplicationsTableViewComponent } from './campaign-details/campaign-applications-table-view/campaign-applications-table-view.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import {
  NgxMatDatetimePickerModule,
  NgxMatNativeDateModule,
  NgxMatTimepickerModule,
} from '@angular-material-components/datetime-picker';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTooltipModule } from '@angular/material/tooltip';
import { SharedModule } from '../shared/shared.module';
import { MatTabsModule } from '@angular/material/tabs';
import { ApprovedApplicationsTableViewComponent } from './campaign-details/approved-applications-table-view/approved-applications-table-view.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { ApplicantsProofSidenavComponent } from './campaign-details/applicants-proof-sidenav/applicants-proof-sidenav.component';
import { CampaignAplicationDetailsTVComponent } from './campaign-details/campaign-aplication-details-t-v/campaign-aplication-details-t-v.component';
import { NgxMasonryModule } from 'ngx-masonry';

@NgModule({
  declarations: [
    CampaignComponent,
    CampaignFormComponent,
    CampaignCardViewComponent,
    CardListComponent,
    CampaignDetailsComponent,
    CampaignApplicationsTableViewComponent,
    ApprovedApplicationsTableViewComponent,
    ApplicantsProofSidenavComponent,
    CampaignAplicationDetailsTVComponent,
  ],
  imports: [
    SharedModule,
    CommonModule,
    CampaignRoutingModule,
    MatCardModule,
    MatIconModule,
    MatMenuModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatNativeDateModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatNativeDateModule,
    MatDialogModule,
    MatAutocompleteModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    NgxMatTimepickerModule,
    NgxMatDatetimePickerModule,
    NgxMatNativeDateModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatIconModule,
    MatTabsModule,
    MatSidenavModule,
    MatButtonModule,
    NgxMasonryModule,
  ],

  exports: [
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    CampaignApplicationsTableViewComponent,
  ],
  entryComponents: [CampaignFormComponent],
  providers: [
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
  ],
})
export class CampaignModule {}
