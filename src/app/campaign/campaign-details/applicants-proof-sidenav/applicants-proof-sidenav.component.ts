import { Component, Injectable, Input, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Subject, takeUntil } from 'rxjs';
import { AplicantObject } from 'src/app/customer/customer.model';
import { ProofFilter } from 'src/app/event/event.model';
import { DataTableResponse } from 'src/app/shared/shared.model';
import { CampaignsAplicantObject, ProofDetails } from '../../campaign.model';
import { CampaignService } from '../../campaign.service';

@Component({
  selector: 'app-applicants-proof-sidenav',
  templateUrl: './applicants-proof-sidenav.component.html',
  styleUrls: ['./applicants-proof-sidenav.component.scss']
})





@Injectable()
export class ApplicantsProofSidenavComponent implements OnInit {

  @Input() aplicant: CampaignsAplicantObject;
  @Input() sidenav: MatSidenav;
  @Input() campaignId: string;
  filter: ProofFilter = new ProofFilter();
  _unsubscribeAll: Subject<void> = new Subject();
  proofList: Array<ProofDetails> = new Array<ProofDetails>();

  constructor(
               private _campaignService: CampaignService,

  ) { }

  ngOnInit(): void {

    //  this.getProofList();
  }

  closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }




  getProofList() {

    this.filter.pageNumber = 1;
    this.filter.pageSize = 5;
    this.filter.sortColumn = '';
    this.filter.sortDirection = '';
    this.filter.accountId = this.aplicant.applicantId;
    this.filter.campaignId =this.campaignId;

    this._campaignService.listProof(this.filter).pipe(takeUntil(this._unsubscribeAll)).subscribe(
      (data: DataTableResponse<Array<ProofDetails>>) => {
        this.proofList = data.data;

      }
    )

  }


}
