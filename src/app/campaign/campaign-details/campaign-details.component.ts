import {
  Component,
  Inject,
  Injectable,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subject, take, takeUntil } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { CustomerService } from 'src/app/customer/customer.service';
import {
  DataTableResponse,
  FilterRatingList,
  RatingDetails,
} from 'src/app/shared/shared.model';
import { SharedService } from 'src/app/shared/shared.service';
import { CompanyDetails } from 'src/app/user/user.model';
import { UserService } from 'src/app/user/user.service';
import Swal from 'sweetalert2';
import {
  CampaignCustomerListResponce,
  CampaignsAplicantObject,
  CampaignsAplications,
  CampaignsDetails,
  FilterDataTable,
} from '../campaign.model';
import { CampaignService } from '../campaign.service';

@Component({
  selector: 'app-campaign-details',
  templateUrl: './campaign-details.component.html',
  styleUrls: ['./campaign-details.component.scss'],
})
export class CampaignDetailsComponent implements OnInit, OnDestroy {
  @ViewChild('sidenav') public sidenav: MatSidenav;

  activeCampaignDetails: CampaignsDetails = new CampaignsDetails();
  campaignId: string = null;
  is_comapny: Boolean = false;
  is_loaded: Boolean = false;
  activeAccountId: string = null;
  companyId: string = null;
  filter: FilterDataTable = new FilterDataTable();
  campaignAplicantsObject: CampaignCustomerListResponce<CampaignsAplicantObject> =
    new CampaignCustomerListResponce<CampaignsAplicantObject>();
  campaignDetails: CampaignsDetails = new CampaignsDetails();
  has_applicants: Boolean = false;
  activeCompany: Boolean = false;
  activeGuid: string = null;
  showSpinner: Boolean = false;
  companyDetails: CompanyDetails = new CompanyDetails();
  private _unsubscribeAll: Subject<void> = new Subject<void>();
  activeCompanyDetails: CompanyDetails = new CompanyDetails();
  is_company: Boolean = false;
  filterRating: FilterRatingList = new FilterRatingList();
  ratingDetailsList: Array<RatingDetails> = new Array<RatingDetails>();

  constructor(
    private _userService: UserService,
    private _sharedService: SharedService,
    private _campaignService: CampaignService,
    private _activatedRoute: ActivatedRoute,
    private _authService: AuthService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _customerService: CustomerService
  ) {}

  ngOnDestroy(): void {
    this._unsubscribeAll.complete();
    this._unsubscribeAll.next();
  }

  ngOnInit(): void {
    this._activatedRoute.params
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((params: Params) => {
        if (params['guid']) {
          this.activeGuid = params['guid'];
          this.showSpinner = true;
          this.checkAccountType();
        }
      });
    this.activeAccountId =
      this._authService.currentActiveAccount.account.accountId;
    console.log('Activ Company ID ---->', this.activeAccountId);
    this.getAccountType();
  }

  getCompanyDetails() {
    if (this._userService.company_details.value.companyId == null) {
      this._userService
        .getCompanyDetails(
          this._authService.currentActiveAccount.account.accountId
        )
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe((data: CompanyDetails) => {
          this.companyDetails = data;

          this._userService.company_details.next(this.companyDetails);
          this.getCampaignDetails();
        });
    } else {
      this.companyDetails = this._userService.company_details.value;
      this.getCampaignDetails();
    }
  }

  getCampaignDetails(): void {
    this._campaignService
      .getCampaignDetails(this.activeGuid)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(
        (campaignDetails: CampaignsDetails) => {
          this.activeCampaignDetails = campaignDetails;

          this.is_loaded = true;

          this.showSpinner = false;

          this.activeCompany =
            this.companyDetails.companyId ==
            this.activeCampaignDetails.companyId;

          this.getRatingList();

          if (
            campaignDetails &&
            campaignDetails.campaignApplications === true
          ) {
            this.has_applicants = true;
            this.campaignId = campaignDetails.campaignId;

            console.log('has_applicants =>', this.has_applicants);
            this.showSpinner = false;
          }
        },
        () => {
          this._router.navigate(['campaign/list']);
        }
      );
  }

  checkAccountType() {
    this._sharedService.is_comapny
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((data: Boolean) => {
        this.is_comapny = data;

        if (this.is_comapny) {
          this.getCompanyDetails();
        } else {
          this.getCampaignDetails();
        }
      });
  }

  applayForCampaign() {
    this.showSpinner = true;
    this._campaignService
      .createCampaignApplication(this.activeCampaignDetails.campaignId)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(
        (data: CampaignsAplications) => {
          if (data) {
            Swal.fire({
              icon: 'success',
              title: 'You applied for this campaign!',
              showConfirmButton: false,
              timer: 2000,
              willClose: () => {},
            });
            this.showSpinner = false;
          }
        },
        (error) => {
          if (
            (error.error.response =
              'You have already applied for this campaign!')
          ) {
            Swal.fire({
              icon: 'error',
              title: 'You have already applied for this campaign!',
              showConfirmButton: false,
              timer: 2000,
              willClose: () => {},
            });
            this.showSpinner = false;
          }
        }
      );
  }
  viewCompanynByGuid() {
    this._router.navigate(
      [`../../../company/details/${this.activeCampaignDetails.companyGuid}`],
      { relativeTo: this._route }
    );
  }

  getRatingList(): void {
    this.filterRating.pageNumber = 1;
    this.filterRating.pageSize = 10;
    this.filterRating.sortDirection = 'asc';
    this.filterRating.sortColumn = 'feedbackId';
    this.filterRating.campaignId = this.activeCampaignDetails.campaignId;
    this.filterRating.accountId = '';

    this._campaignService
      .getRatingList(this.filterRating)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((data: DataTableResponse<Array<RatingDetails>>) => {
        this.ratingDetailsList = data.data;
        console.log('rating details --> ', this.ratingDetailsList);
      });
  }
  removeRating(feedbackId: string) {
    console.log('feedBack ID --------->', feedbackId);

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        this._customerService
          .deleteRating(feedbackId)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe((data: Boolean) => {
            if (data) {
              Swal.fire('Deleted!', 'Your file has been deleted.', 'success');
            }
            this.getCompanyDetails();
          });
      }
    });
  }

  getAccountType() {
    this._sharedService.is_comapny
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((data: Boolean) => {
        this.is_company = data;
      });
  }
}
