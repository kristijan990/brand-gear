import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { merge, Subject, takeUntil } from 'rxjs';
import { CampaignsAplicantObject, FilterDataTable, } from '../../campaign.model';
import { CampaignService } from '../../campaign.service';
import { CampaignApplicationsDataSource } from './campaign-applications-table.datasource';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';



@Component({
  selector: 'app-campaign-applications-table-view',
  templateUrl: './campaign-applications-table-view.component.html',
  styleUrls: ['./campaign-applications-table-view.component.scss']
})
export class CampaignApplicationsTableViewComponent implements OnInit {


 _unsubscribeAll: Subject<void> = new Subject();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  displayedColumns = ['applicantFirstName', 'applicantLastName', 'applicantPhoneNumber', 'dateCreated', 'campaignApplicationStatusName', 'buttons'];

  dataSource: CampaignApplicationsDataSource;
  filter: FilterDataTable = new FilterDataTable();
  totalCount: number = 0;
  show_spinner: boolean = true;
  show_table: boolean = false;

  aplicant: CampaignsAplicantObject = new CampaignsAplicantObject();

  @Input() campaignId: string;



  constructor(
               private _campaignService: CampaignService,
               private _router: Router,
               private _route: ActivatedRoute

  ) { }

  ngOnInit(): void {
    if(this.campaignId){
      this.dataSource = new CampaignApplicationsDataSource(this._campaignService);

      this.filter.pageNumber = 1;
      this.filter.pageSize = 5;
      this.filter.sortColumn = '';
      this.filter.sortDirection = '';
      this.filter.campaignId = this.campaignId;
      this.filter.campaignApplicationStatus = "";

      this.dataSource.getApiData(this.filter);

      // Recieve api data
      this.dataSource.dataTable.pipe(takeUntil(this._unsubscribeAll)).subscribe(
        (data: CampaignsAplicantObject[]) => {
        console.log("aplicant data -->", data)
         if (data.length > 0) {
            this.show_table = true;

          }


        }
      )

      // Recieve total items ( number of rows )
      this.dataSource.totalCount.pipe(takeUntil(this._unsubscribeAll)).subscribe(
        (data: number) => {
          this.totalCount = data;
        }
      );
      // Show/hide spinner
      this.dataSource.loading.pipe(takeUntil(this._unsubscribeAll)).subscribe(
        (data: boolean) => {
          this.show_spinner = data;
        }
      );
    }

  }

  acceptAplicant(application: CampaignsAplicantObject) {

    if(application.campaignApplicationStatusName == 'Pending' || application.campaignApplicationStatusName == 'Declined') {

      this._campaignService.acceptCampaignApplication(application.campaignApplicationId).pipe(takeUntil(this._unsubscribeAll)).subscribe(
        (data: Boolean) => {
          Swal.fire({
            title: 'Do you want to accept this applicant?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: 'Accept',
            denyButtonText: `Don't accept`,
          }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
              Swal.fire('Applicant Accepted!', '', 'success')
            } else if (result.isDenied) {
              Swal.fire('Changes are not saved', '', 'info')
            }
          })
          this.reLoadTable();

          console.log("acceptAplicant", data)
        }
      )

    }
  }


  declineAplicant(application: CampaignsAplicantObject) {


    if(application.campaignApplicationStatusName == 'Approved' || application.campaignApplicationStatusName == 'Pending') {
      this._campaignService.declinedCampaignApplication(application.campaignApplicationId).pipe(takeUntil(this._unsubscribeAll)).subscribe(
        (data: Boolean) => {
          Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, decline it!'
          }).then((result) => {
            if (result.isConfirmed) {
              Swal.fire(
                'Deleted!',
                'Your applincant has been decline.',
                'success'
              )
            }  this.reLoadTable();
          })


          console.log("acceptAplicant", data)
        }
      )
    }


  }

  getAplicantProfile(row: any): void {
    this._router.navigate([`../../../customer/details/${row.applicantGuid}`], {relativeTo: this._route })

    console.log("guid" , row.applicantGuid)
  }



  ngOnDestroy(): void {
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
  }

  ngAfterViewInit(): void {
      merge(this.sort.sortChange, this.paginator.page).pipe(takeUntil(this._unsubscribeAll)).subscribe(
        data => {
          this.reLoadTable();
        }
      )
  }

  reLoadTable() {
    this.filter.pageNumber = this.paginator.pageIndex + 1;
    this.filter.pageSize = this.paginator.pageSize;
    this.filter.sortColumn = this.sort.active;
    this.filter.sortDirection = this.sort.direction;

    this.dataSource.getApiData(this.filter);
  }

}
