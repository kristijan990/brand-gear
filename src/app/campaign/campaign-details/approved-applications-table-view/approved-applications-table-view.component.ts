import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSidenav } from '@angular/material/sidenav';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute, Router } from '@angular/router';
import { merge, Subject, takeUntil } from 'rxjs';
import { CampaignsAplicantObject, FilterDataTable } from '../../campaign.model';
import { CampaignService } from '../../campaign.service';
import { ApplicantsProofSidenavComponent } from '../applicants-proof-sidenav/applicants-proof-sidenav.component';
import { ApprovedAplicationsTableDataSource } from './approved-applications-table-view.datasource';

@Component({
  selector: 'app-approved-applications-table-view',
  templateUrl: './approved-applications-table-view.component.html',
  styleUrls: ['./approved-applications-table-view.component.scss']
})
export class ApprovedApplicationsTableViewComponent implements OnInit {


  // @ViewChild('sidenav') public sidenav: MatSidenav;
  @Input() sidenav: MatSidenav;

 _unsubscribeAll: Subject<void> = new Subject();

 @ViewChild(MatPaginator) paginator: MatPaginator;
 @ViewChild(MatSort) sort: MatSort;
 displayedColumns = ['applicantFirstName', 'applicantLastName', 'applicantPhoneNumber', 'dateCreated', 'proof'];
 showFiller = false;
 dataSource: ApprovedAplicationsTableDataSource;
 filter: FilterDataTable = new FilterDataTable();
 totalCount: number = 0;
 show_spinner: boolean = true;
 show_table: boolean = false;

 aplicant: CampaignsAplicantObject = new CampaignsAplicantObject();

 @Input() campaignID: string;




 constructor(
              private _campaignService: CampaignService,
              private _router: Router,
              private _route: ActivatedRoute,

 ) { }



 ngOnInit(): void {
   if(this.campaignID){
     this.dataSource = new ApprovedAplicationsTableDataSource(this._campaignService);

     this.filter.pageNumber = 1;
     this.filter.pageSize = 5;
     this.filter.sortColumn = '';
     this.filter.sortDirection = '';
     this.filter.campaignId = this.campaignID;
     this.filter.campaignApplicationStatus = "Approved";

     this.dataSource.getApiData(this.filter);

     // Recieve api data
     this.dataSource.dataTable.pipe(takeUntil(this._unsubscribeAll)).subscribe(
       (data: CampaignsAplicantObject[]) => {
       console.log("aplicant data -->", data)
        if (data.length > 0) {

           this.show_table = true;


         }



       }
     )

     // Recieve total items ( number of rows )
     this.dataSource.totalCount.pipe(takeUntil(this._unsubscribeAll)).subscribe(
       (data: number) => {
         this.totalCount = data;
       }
     );
     // Show/hide spinner
     this.dataSource.loading.pipe(takeUntil(this._unsubscribeAll)).subscribe(
       (data: boolean) => {
         this.show_spinner = data;
       }
     );
   }

 }





 getAplicantProfile(row: any): void {
   this._router.navigate([`../../../customer/details/${row.applicantGuid}`], {relativeTo: this._route })

   console.log("guid" , row.applicantGuid)
 }



 ngOnDestroy(): void {
     this._unsubscribeAll.next();
     this._unsubscribeAll.complete();
 }

 ngAfterViewInit(): void {
     merge(this.sort.sortChange, this.paginator.page).pipe(takeUntil(this._unsubscribeAll)).subscribe(
       data => {
         this.reLoadTable();
       }
     )
 }

 reLoadTable() {
   this.filter.pageNumber = this.paginator.pageIndex + 1;
   this.filter.pageSize = this.paginator.pageSize;
   this.filter.sortColumn = this.sort.active;
   this.filter.sortDirection = this.sort.direction;

   this.dataSource.getApiData(this.filter);
 }

  openNav() {
  document.getElementById("mySidenav").style.width = "250px";
}

/* Set the width of the side navigation to 0 */
 closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}


}
