import { DataSource } from "@angular/cdk/collections";
import { BehaviorSubject, Observable, Subject, takeUntil } from "rxjs";
import { CampaignCustomerListResponce, CampaignsAplicantObject, CampaignsAplications, FilterDataTable } from "../../campaign.model";
import { CampaignService } from "../../campaign.service";


export class ApprovedAplicationsTableDataSource implements DataSource<CampaignsAplications[]> {

  dataTable = new BehaviorSubject<CampaignsAplicantObject[]>([]);
  totalCount= new BehaviorSubject<number>(0);
  loading = new BehaviorSubject<boolean>(true);
  private _unsubscribeAll: Subject<void> = new Subject<void>();

  constructor(
      private _campaignService: CampaignService,
  ) { }


  getApiData(filter: FilterDataTable) {
      this.loading.next(true);

      this._campaignService.getCampaignAplicant(filter).pipe(takeUntil(this._unsubscribeAll)).subscribe(
        (data: CampaignCustomerListResponce<CampaignsAplicantObject> | string) => {

          console.log('table data -> ',data)
          if(typeof data != 'string'){
            this.dataTable.next(data.data);
            console.log('data.data', data.data)

            this.totalCount.next(data.totalRecords);


          }
          this.loading.next(false);


        }

      )
  }

  connect(): Observable<any> {
      return this.dataTable;
  }

  disconnect(): void {
      this.dataTable.complete();
  }

}
