import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject, takeUntil, merge } from 'rxjs';
import Swal from 'sweetalert2';
import {
  FilterDataTable,
  CampaignsAplicantObject,
  OpenRatingDialogDataObject,
} from '../../campaign.model';
import { CampaignService } from '../../campaign.service';
import { CampaignApplicationsDataSource } from '../campaign-applications-table-view/campaign-applications-table.datasource';
import { CampaignAplicationsTVDataSource } from './campaign-aplication-t-v.datasource';
import { Output, EventEmitter } from '@angular/core';
import { SharedService } from 'src/app/shared/shared.service';
import { MatSidenav } from '@angular/material/sidenav';
import { RatingFormComponent } from 'src/app/shared/rating-form/rating-form.component';
import { MatDialog } from '@angular/material/dialog';
import { CustomerDetailsObject } from 'src/app/user/user.model';
import { CampaignDetailsComponent } from '../campaign-details.component';

@Component({
  selector: 'app-campaign-aplication-details-t-v',
  templateUrl: './campaign-aplication-details-t-v.component.html',
  styleUrls: ['./campaign-aplication-details-t-v.component.scss'],
})
export class CampaignAplicationDetailsTVComponent implements OnInit, OnChanges {
  _unsubscribeAll: Subject<void> = new Subject();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @Input() type: string;
  @Input() campaignId: string;
  @Input() sidenav: MatSidenav;

  displayedColumns: string[] = [
    'applicantFirstName',
    'applicantLastName',
    'applicantPhoneNumber',
    'dateCreated',
    'campaignApplicationStatusName',
    'buttons',
  ];
  // displayedColumnsAplicants = ['applicantFirstName', 'applicantLastName', 'applicantPhoneNumber', 'dateCreated', 'proof']
  @Output() newItemEvent = new EventEmitter<string>();
  dataSource: CampaignAplicationsTVDataSource;
  filter: FilterDataTable = new FilterDataTable();
  filterApproved: FilterDataTable = new FilterDataTable();
  totalCount: number = 0;
  show_spinner: boolean = true;
  show_table: boolean = false;
  // aplicants_table: Boolean = false;
  // participants_table: Boolean = false;
  aplicant: CampaignsAplicantObject = new CampaignsAplicantObject();
  acceptApllicantsList: Array<CampaignsAplicantObject> =
    new Array<CampaignsAplicantObject>();
  participantsTable: Boolean = false;

  constructor(
    private _campaignService: CampaignService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _sharedService: SharedService,
    public dialog: MatDialog,
    private _campaignDetails: CampaignDetailsComponent
  ) {}
  ngOnChanges(changes: SimpleChanges): void {
    if (changes['type']) {
      let campaignApplicationStatusNameColumn = this.displayedColumns.find(
        (x) => x === 'campaignApplicationStatusName'
      );
      let buttonsColumn = this.displayedColumns.find((x) => x === 'buttons');
      let proofColumn = this.displayedColumns.find((x) => x === 'proof');

      if (changes['type'].currentValue == 'All') {
        if (proofColumn) {
          this.displayedColumns.splice(
            this.displayedColumns.indexOf(proofColumn),
            1
          );
        }

        if (!campaignApplicationStatusNameColumn) {
          this.displayedColumns.push('campaignApplicationStatusName');
        }

        if (!buttonsColumn) {
          this.displayedColumns.push('buttons');
        }
      } else if (changes['type'].currentValue == 'Approved') {
        if (campaignApplicationStatusNameColumn) {
          this.displayedColumns.splice(
            this.displayedColumns.indexOf(campaignApplicationStatusNameColumn),
            1
          );
        }

        if (buttonsColumn) {
          this.displayedColumns.splice(
            this.displayedColumns.indexOf(buttonsColumn),
            1
          );
        }

        if (!proofColumn) {
          this.displayedColumns.push('proof');
        }
      }
    }
  }

  ngOnInit(): void {
    if (this.campaignId) {
      this.dataSource = new CampaignAplicationsTVDataSource(
        this._campaignService
      );

      this.filter.pageNumber = 1;
      this.filter.pageSize = 5;
      this.filter.sortColumn = '';
      this.filter.sortDirection = '';
      this.filter.campaignId = this.campaignId;
      this.filter.campaignApplicationStatus =
        this.type === 'Approved' ? this.type : '';

      this.getData();

      // Recieve total items ( number of rows )
      this.dataSource.totalCount
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe((data: number) => {
          this.totalCount = data;
        });
      // Show/hide spinner
      this.dataSource.loading
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe((data: boolean) => {
          this.show_spinner = data;
        });
    }
  }

  getData(): void {
    this.dataSource.getApiData(this.filter);
  }

  acceptAplicant(application: CampaignsAplicantObject) {
    if (
      application.campaignApplicationStatusName == 'Pending' ||
      application.campaignApplicationStatusName == 'Declined'
    ) {
      this._campaignService
        .acceptCampaignApplication(application.campaignApplicationId)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe((data: Boolean) => {
          Swal.fire({
            title: 'Do you want to accept this applicant?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: 'Accept',
            denyButtonText: `Don't accept`,
          }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
              Swal.fire('Applicant Accepted!', '', 'success');
            } else if (result.isDenied) {
              Swal.fire('Changes are not saved', '', 'info');
            }
          });
          this.reLoadTable();

          console.log('acceptAplicant', data);
        });
    }
  }

  declineAplicant(application: CampaignsAplicantObject) {
    if (
      application.campaignApplicationStatusName == 'Approved' ||
      application.campaignApplicationStatusName == 'Pending'
    ) {
      this._campaignService
        .declinedCampaignApplication(application.campaignApplicationId)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe((data: Boolean) => {
          Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, decline it!',
          }).then((result) => {
            if (result.isConfirmed) {
              Swal.fire(
                'Deleted!',
                'Your applincant has been decline.',
                'success'
              );
            }
            this.reLoadTable();
          });

          console.log('acceptAplicant', data);
        });
    }
  }

  getAplicantProfile(row: any): void {
    this._router.navigate([`../../../customer/details/${row.applicantGuid}`], {
      relativeTo: this._route,
    });

    console.log('guid', row.applicantGuid);
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  ngAfterViewInit(): void {
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((data) => {
        this.reLoadTable();
      });
  }

  reLoadTable() {
    this.filter.pageNumber = this.paginator.pageIndex + 1;
    this.filter.pageSize = this.paginator.pageSize;
    this.filter.sortColumn = this.sort.active;
    this.filter.sortDirection = this.sort.direction;

    this.dataSource.getApiData(this.filter);
  }

  openNav() {
    document.getElementById('mySidenav').style.width = '250px';
  }

  /* Set the width of the side navigation to 0 */
  closeNav() {
    document.getElementById('mySidenav').style.width = '0';
  }

  openRatingForm(applicant: CampaignsAplicantObject) {
    let object_to_dialog: OpenRatingDialogDataObject =
      new OpenRatingDialogDataObject();
    object_to_dialog.campaign_id = this.campaignId;
    object_to_dialog.applicant = applicant;

    const dialogRef = this.dialog.open(RatingFormComponent, {
      data: object_to_dialog,
      width: '600px',
      height: '400px',
    });
    dialogRef.afterClosed().subscribe((response) => {
      if (response) {
        this._campaignDetails.getRatingList();
      }
    });
  }
}
