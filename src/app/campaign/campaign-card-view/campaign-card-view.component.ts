import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { AccountObject } from 'src/app/auth/auth.model';
import { AuthService } from 'src/app/auth/auth.service';
import { ApiResult } from 'src/app/shared/shared.model';
import { SharedService } from 'src/app/shared/shared.service';
import { CampaignApplications, CompanyDetails } from 'src/app/user/user.model';
import { UserService } from 'src/app/user/user.service';
import { CampaignDetailsComponent } from '../campaign-details/campaign-details.component';
import { CampaignsAplications, CampaignsDetails } from '../campaign.model';

import { MatDialog } from '@angular/material/dialog';

import Swal from 'sweetalert2';
import { CampaignFormComponent } from '../campaign-form/campaign-form.component';

import { CampaignService } from '../campaign.service';
import { CardListComponent } from '../card-list/card-list.component';

@Component({
  selector: 'app-campaign-card-view',
  templateUrl: './campaign-card-view.component.html',
  styleUrls: ['./campaign-card-view.component.scss'],
})
export class CampaignCardViewComponent implements OnInit {
  private _unsubscribeAll: Subject<void> = new Subject<void>();
  bgImage = 'string';
  showSpinner: boolean = false;
  is_saving_results: boolean = false;
  has_picture: Boolean = false;

  @Input() list: CampaignsDetails;
  // @Input() campaignDetails: CampaignsDetails;


  guid: string = null;
  is_company: Boolean = false;

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _campaignService: CampaignService,
    private _authService: AuthService,
    private _userService: UserService,
    private _sharedService: SharedService,
    private _cardList: CardListComponent,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.checkAccountType();
    console.log('campaignID', this.list.campaignId);
  }

  viewCampaignByGuid() {
    this._router.navigate([`../details/${this.list.campaignGuid}`], {
      relativeTo: this._route,
    });
  }

  checkAccountType() {
    this._sharedService.is_comapny
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((data: Boolean) => {
        this.is_company = data;
      });
  }

  applyForCampaign() {
    this.showSpinner = true;
    this._campaignService
      .createCampaignApplication(this.list.campaignId)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(
        (data: CampaignsAplications) => {
          if (data) {
            Swal.fire({
              icon: 'success',
              title: 'You applied for this campaign!',
              showConfirmButton: false,
              timer: 2000,
              willClose: () => {},
            });
            this.showSpinner = false;
          }
        },
        (error) => {
          console.log('error', error.error.response);
          if (
            (error.error.response =
              'You have already applied for this campaign!')
          ) {
            Swal.fire({
              icon: 'error',
              title: 'You have already applied for this campaign!',
              showConfirmButton: false,
              timer: 2000,
              willClose: () => {},
            });
            this.showSpinner = false;
          }
        }
      );
  }

  editCampaign(list: CampaignsDetails) {

    const dialogRef = this.dialog.open(CampaignFormComponent, {
      data: list,
    });

    console.log('list  ---->', list);
    dialogRef.afterClosed().subscribe((response) => {

      if (response) {
        Swal.fire({
          icon: 'success',
          title: 'successfully changed',
          showConfirmButton: false,
          timer: 2000,
          willClose: () => {
            this.is_saving_results = false;
            this._campaignService.getCampaignDetails(list.campaignGuid);
          },
        });
        this._campaignService.getCampaignDetails(list.campaignGuid);
        // this._campaignService.listCampaign()
      }
    });
  }

  deleteCampaign(campaignId: string) {
    console.log('delete');
    Swal.fire({
      title: 'Are you sure you want to delete this campaign?',

      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'No',
      confirmButtonText: 'Yes',
    }).then((result) => {
      if (result.isConfirmed) {
        this.is_saving_results = true;
        this._cardList.getCampaignList();

        this._campaignService
          .deleteCampaign(campaignId)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(
            () => {
              Swal.fire({
                icon: 'success',
                title: 'successfully deleted!',
                showConfirmButton: false,
                timer: 2000,
                willClose: () => {
                  this.is_saving_results = false;
                  this._cardList.getCampaignList();
                },
              });
            },
            () => {
              Swal.fire({
                icon: 'error',
                title: 'something wrong , try again!',
                showConfirmButton: false,
                timer: 2000,
                willClose: () => {
                  this.is_saving_results = false;
                },
              });
            }
          );
      }
    });
  }

  addCampaign(campaign = CampaignsDetails) {
    const dialogRef = this.dialog.open(CampaignFormComponent, {
      width: '600px',
      data: campaign,
    });
    dialogRef.afterClosed().subscribe((response) => {
      if (response) {
      }
    });
  }
}
