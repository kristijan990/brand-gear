import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { DataTableFilter, DataTableResponse } from 'src/app/shared/shared.model';
import { SharedService } from 'src/app/shared/shared.service';
import { CampaignFormComponent } from '../campaign-form/campaign-form.component';
import { CampaignsDetails } from '../campaign.model';
import { CampaignService } from '../campaign.service';
import Swal from 'sweetalert2';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import { UserService } from 'src/app/user/user.service';

@Component({
  selector: 'app-card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.scss'],

})
export class CardListComponent implements OnInit, OnDestroy {
  filter: DataTableFilter = new DataTableFilter();
  campaignsList: Array<CampaignsDetails> = new Array<CampaignsDetails>();
  private _unsubscribeAll: Subject<void> = new Subject<void>();
  is_company: Boolean = false;
  showSpinner: boolean = false;
  campaignList: CampaignsDetails[];
  is_saving_results: Boolean;
  campaignId: string = null;
  list: CampaignsDetails = new CampaignsDetails();
  account_id: string = '';
  company_id: string = '';

  constructor(
    private _campaignService: CampaignService,
    private _sharedService: SharedService,
    public dialog: MatDialog,
    private _router: Router,
    private _route: ActivatedRoute,
    private _authService: AuthService,
    private _userService: UserService
  ) {}

  ngOnInit(): void {
    this.showSpinner = true;
    this.getCampaignList();
    this.checkAccountType();
    this.account_id = this._authService.currentActiveAccount.account.accountId;
    this.company_id = this._userService.company_details.value.companyId;
    console.log('company id -> ', this.company_id);

    // this._userService.company_details.pipe(takeUntil(this._unsubscribeAll))
    // .subscribe((data: any) => {

    // })
  }


  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }


  addCampaign() {
    const dialogRef = this.dialog.open(CampaignFormComponent, {
      width: '600px',
      data : null
    });
    dialogRef.afterClosed().subscribe((response) => {
      if (response) {
      }
    });
  }

  getCampaignList(): void {
    this.filter.pageNumber = 1;
    this.filter.pageSize = 10;
    this.filter.sortDirection = 'asc';
    this.filter.sortColumn = 'campaignId';

    this._campaignService
      .listCampaign(this.filter)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((data: DataTableResponse<Array<CampaignsDetails>>) => {

        this.campaignsList = data.data;
       // this._sharedService.is_saving_results.next(true);
        this.showSpinner = false;
      });
  }

  checkAccountType(){
    this._sharedService.is_comapny.pipe(takeUntil(this._unsubscribeAll)).subscribe(
   (data: Boolean) => {
     this.is_company = data;
   }
 )


 }

  loadCampaign() {
    this.showSpinner = true;
    this._campaignService
      .listCampaign(this.filter)
      .subscribe((response: any) => {
        this.campaignList = response.data;
        this.showSpinner = false;
      });
  }

  addNewCampaign(list: CampaignsDetails) {
    const dialogRef = this.dialog.open(CampaignFormComponent, {
      data: list,
    });
    dialogRef.afterClosed().subscribe((response) => {
      if (response) {
        this.getCampaignList();
      }
    });

  }

  editCampaign(list: CampaignsDetails) {

    const dialogRef = this.dialog.open(CampaignFormComponent, {
      data: list,
    });

    console.log('list  ---->', list);
    dialogRef.afterClosed().subscribe((response) => {

      if (response) {
        Swal.fire({
          icon: 'success',
          title: 'Campaign was successfuly updated',
          showConfirmButton: false,
          timer: 2000,
          willClose: () => {
            this.is_saving_results = false;
            this._campaignService.getCampaignDetails(list.campaignGuid);
          },
        });
        this._campaignService.getCampaignDetails(list.campaignGuid);
        this.getCampaignList();
      }
    });
  }

  deleteCampaign(campaignId: string) {
    console.log('delete');
    Swal.fire({
      title: 'Are you sure?',

      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'No',
      confirmButtonText: 'Yes',
    }).then((result) => {
      if (result.isConfirmed) {
        this.is_saving_results = true;
        this.getCampaignList();

        this._campaignService
          .deleteCampaign(campaignId)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(
            () => {
              Swal.fire({
                icon: 'success',
                title: 'Campaign successfuly deleted!',
                showConfirmButton: false,
                timer: 2000,
                willClose: () => {
                  this.is_saving_results = false;
                  this.getCampaignList();
                },
              });
            },
            () => {
              Swal.fire({
                icon: 'error',
                title: 'Something went wrong, please try again!',
                showConfirmButton: false,
                timer: 2000,
                willClose: () => {
                  this.is_saving_results = false;
                },
              });
            }
          );
      }
    });
  }

}
