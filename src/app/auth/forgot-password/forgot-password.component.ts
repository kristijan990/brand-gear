import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import Swal from 'sweetalert2';
import { AccountObject } from '../auth.model';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
})
export class ForgotPasswordComponent implements OnInit, OnDestroy {
  emailFormGroup: FormGroup;
  changePasswordFormGroup: FormGroup;

  showSpinner: Boolean = false;
  isEmailValid: Boolean = true;
  resetPassword: Boolean = false;
  accountId: string = null;
  wrongCode: Boolean = false;

  private _unsubscribeAll: Subject<void> = new Subject<void>();

  constructor(
    public _formBuilder: FormBuilder,
    private _authService: AuthService,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this.initMailFormGroup();
    this.initResetMailFormGroup();
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.complete();
    this._unsubscribeAll.next();
  }

  initMailFormGroup() {
    return (this.emailFormGroup = this._formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
    }));
  }

  initResetMailFormGroup() {
    return (this.changePasswordFormGroup = this._formBuilder.group({
      code: ['', [Validators.required]],
      password: ['', [Validators.required]],
    }));
  }

  public forgotPassword() {
    this.showSpinner = true;
    //SAMO EMAIL: STRING
    let email: string = this.emailFormGroup.get('email').value;

    console.log(this.emailFormGroup.get('email').value);

    // this._authService
    //   .forgotMail(this.editFormGroup.get('email').value)
    //   .pipe()
    //   .subscribe((data: AccountObject) => {});

    this._authService
      .forgotPassword(email)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((data: AccountObject) => {
        this.accountId = data.accountId;

        // NEMASH PROVERKA DALI E USPESHNO PRATEN EMAILOT I NE TREBA OVDE DA GO NAVIGIRASH NA AUTH/LOGIN
        // OVDE TREBA AKO E USPESHNO DA MU JA POKAZHESH DRUGATA FORMA
        Swal.fire({
          icon: 'success',
          title:
            'Your mail was reset successful. Please check your email for verification!',
          showConfirmButton: false,
          timer: 2000,
          willClose: () => {
            this.resetPassword = true;
          },
        });
        this.showSpinner = false;
      });
  }

  public changePassword() {
    this.showSpinner = true;

    let code = this.changePasswordFormGroup.get('code').value;
    let password = this.changePasswordFormGroup.get('password').value;

    this._authService
      .resetPassword(this.accountId, code, password)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(
        (data: AccountObject) => {
          if (data.accountId) {
            Swal.fire({
              icon: 'success',
              title: 'Your password was reset successfuly!',
              showConfirmButton: false,
              timer: 2000,
              willClose: () => {
                this._router.navigate(['auth/login']);
              },
            });
          }
        },
        (error) => {
          console.log('error', error.error.response);
          if (
            (error.error.response =
              'Account with that Id is not existing, or your reset code is incorrect.')
          ) {
            let codeWrong = document.getElementById('wrong-code');
            this.wrongCode = true;
            codeWrong.style.display = 'block';
          }
        }
      );

    this.showSpinner = false;
  }

  checkEmail() {
    let email = this.emailFormGroup.getRawValue().email;

    let invalidEmail = document.getElementById('alert-message');
    this._authService
      .checkEmailStatus(email)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((data: Boolean) => {
        if (data == true) {
          this.isEmailValid = false;
          invalidEmail.style.display = 'block';
        } else if (data == false) {
          this.isEmailValid = true;
          invalidEmail.style.display = 'none';
        }
      });

    //OVDE NE TREBA OVA,
    // TUKU VO TOJ MOMENT KOGA APITO KJE VRATI ODGOVOR
    // DEKA EMAILOT SO CODE ZA PROMENA NA PASSWORD E PRATEN
    // this.resetPassword = true;
  }
}
