import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor(
              private _authService: AuthService,
              private _cookieService: CookieService,
             private _router: Router,
  ) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    // Дали има token во cookie ?
    let token = this._cookieService.get("BranGearToken");
    if (token && token != '') {
      // Да
          // Провери дали претходно сме го наполниле објектот со податоци
          // - ако да, тогаш authguard-от ќе навигира на home
          // - ако не, тогаш ќе се повика API за автентификација со токен
          if (this._authService.currentActiveAccount && this._authService.currentActiveAccount.account && this._authService.currentActiveAccount.account.accountId) {
            return true;
          } else {
            return this._authService.authenthicateWithToken(token);
          }
    } else
    {
      // Не, оди на login
      this._router.navigate(['auth/login']);
    }

    return true;
  }
}
