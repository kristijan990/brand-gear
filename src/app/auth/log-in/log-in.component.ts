import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { SharedService } from 'src/app/shared/shared.service';
import Swal from 'sweetalert2';
import { Account, AccountLoginObject, AccountObject } from '../auth.model';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss'],
})
export class LogInComponent implements OnInit, OnDestroy {

  loginFormGroup: FormGroup;
  isEmailValid: Boolean = true;
  wrongPassword: Boolean = false;
  showSpinner: Boolean = false;
  fieldTextType: Boolean = true;
  private _unsubscribeAll: Subject<void>  = new Subject<void>();

  constructor(
               public _formBuilder: FormBuilder,
               private _authService: AuthService,
               private _router: Router,
               private  _activeRoute: ActivatedRoute,
               private _sharedService: SharedService
  ) {}

  ngOnInit(): void {
    this.initLoginForm();
  }


  ngOnDestroy(): void {
    this._unsubscribeAll.complete();
    this._unsubscribeAll.next();
  }


  initLoginForm() {
    return (this.loginFormGroup = this._formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    }));
  }

  public loginAccount() {
    let accountObject = new AccountLoginObject();
    accountObject.email = this.loginFormGroup.get('email').value;
    accountObject.password = this.loginFormGroup.get('password').value;
    this.showSpinner = true;


    this._authService
      .loginCustomer(accountObject)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((data: Account) => {
        this._authService.storeUserData(data);


      },
      error => {

         console.log("error", error.error.response);
         if(error.error.response == "Your account is not verified."){
          Swal.fire({
            icon: 'error',
            title: 'Your account is not verified. Check your email!',
            showConfirmButton: false,
            timer: 2000,
            willClose: () => {

            },
          });
         }
         else if(error.error.response == "Wrong credentials!"){
          this._sharedService.is_loaded.next(true);
          this.showSpinner = false;
          let passwordWrong = document.getElementById('wrong-password');
          this.wrongPassword = true;
          passwordWrong.style.display = "block";

         }

      });

  }


  checkEmail(event: any) {
    let email = this.loginFormGroup.getRawValue().email;

    let invalidEmail = document.getElementById('alert-message');
    this._authService
      .checkEmailStatus(email)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((data: Boolean) => {
        if (data == true) {
          this.isEmailValid = false;
          invalidEmail.style.display = 'block';
        } else if (data == false) {
          this.isEmailValid = true;
          invalidEmail.style.display = 'none';
        }
      });
  }

  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }

 }
