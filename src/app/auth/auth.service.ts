import { _isTestEnvironment } from '@angular/cdk/platform';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, map, Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ApiResult } from '../shared/shared.model';
import {
  Account,
  AccountCreateObject,
  AccountLoginObject,
  AccountObject,
  CustomerFootPrintObject,
  ForgotMail,
} from './auth.model';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { AppService } from '../app.service';

import { SharedService } from '../shared/shared.service';
import { ThrowStmt } from '@angular/compiler';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  currentActiveAccountSubject: BehaviorSubject<Account> =
    new BehaviorSubject<Account>(new Account());
  currentActiveAccount: Account = new Account();
  token = this._cookieService.get('BranGearToken');

  constructor(
    private _httpClient: HttpClient,
    private _cookieService: CookieService,
    private _router: Router,
    private _sharedService: SharedService
  ) {}

  tokenHeader() {
    let $tokenCookie = this._cookieService.get('BranGearToken');
    if (typeof $tokenCookie !== 'undefined' && $tokenCookie !== '') {
      let header = new HttpHeaders({
        accept: 'application/json',
        Authorization: $tokenCookie,
      });
      return header;
    }

    return new HttpHeaders();
  }

  public registerCustomer(
    customer: AccountCreateObject
  ): Observable<AccountObject> {
    return this._httpClient
      .post<ApiResult<AccountObject>>(
        `${environment.apiUrl}account/create`,
        customer
      )
      .pipe(map((data: ApiResult<AccountObject>) => data.response));
  }

  public loginCustomer(customerLogin: AccountLoginObject): Observable<Account> {
    this._sharedService.is_loaded.next(false);
    return this._httpClient
      .post<ApiResult<Account>>(
        `${environment.apiUrl}account/login`,
        customerLogin
      )
      .pipe(
        map((data: ApiResult<Account>) => {
          console.log('response', data);
          this.storeUserData(data.response);
          this._cookieService.delete('BranGearToken', '/');
          this._cookieService.set(
            'BranGearToken',
            data.response.token,
            1000000000000,
            '/'
          );
          if (
            data.response.account.accountDetailsStatusName == 'NotCompleted'
          ) {
            this._router.navigate(['../../user/details/edit-details']);
            this._sharedService.is_account_competed.next(false);
            this._router.navigate(['user/details/overview']);
          } else {
            this._sharedService.is_account_competed.next(true);
            this._router.navigate(['campaign/list']);
          }
          if (data.response.account.accountTypeName == 'Company') {
            this._sharedService.is_comapny.next(true);
          } else {
            this._sharedService.is_comapny.next(false);
          }
          this._sharedService.is_loaded.next(true);
          return data.response;
        })
      );
  }

  authenthicateWithToken(token: string): Observable<boolean> {
    this._sharedService.is_loaded.next(false);
    let header = new HttpHeaders({
      Authorization: token,
    });
    return this._httpClient
      .get<any>(`${environment.apiUrl}account/authorize`, { headers: header })
      .pipe(
        map((data: ApiResult<Account>) => {
          this.storeUserData(data.response);
          this._sharedService.is_loaded.next(true);
          if (data.response.account.accountTypeName == 'Company') {
            this._sharedService.is_comapny.next(true);
          } else {
            this._sharedService.is_comapny.next(false);
          }
          if (data.response.account.accountDetailsStatusName == 'Completed') {
            this._sharedService.is_account_competed.next(true);
          }
          return true;
        }),

        catchError(this._handleError)
      );
  }

  storeUserData(data: Account) {
    // 1. Зачувај ги податоците во објект
    let _previousToken = this.token; // ?
    this.currentActiveAccount = data;
    this.currentActiveAccount.token = this.currentActiveAccount.token
      ? this.currentActiveAccount.token
      : _previousToken; // ?

    // 2. Зачувај го токенот во cookiе
    // this._cookieService.set("BranGearToken", this.currentActiveAccount.token);

    // 3. Проследи го објектот со податоци на BehaviorSubject за глобална употреба
    this.currentActiveAccountSubject.next(this.currentActiveAccount);
  }

  logOut() {
    this._cookieService.delete('BranGearToken');
    this.currentActiveAccount = new Account();
    this.currentActiveAccountSubject.next(this.currentActiveAccount);
    this._router.navigate(['/landing']);
  }

  public verifyAccount(guid: string): Observable<Boolean> {
    return this._httpClient
      .get<ApiResult<Boolean>>(
        `${environment.apiUrl}account/verify?guid=${guid}`
      )
      .pipe(map((data: ApiResult<Boolean>) => data.response));
  }

  public checkEmailStatus(email: string): Observable<Boolean> {
    return this._httpClient
      .get<ApiResult<Boolean>>(
        `${environment.apiUrl}account/check/email?email=${email}`
      )
      .pipe(map((data: ApiResult<Boolean>) => data.response));
  }
  public forgotPassword(email: string): Observable<AccountObject> {
    return this._httpClient
      .get<ApiResult<AccountObject>>(
        `${
          environment.apiUrl
        }account/forgot/password?email=${encodeURIComponent(email)}`
      )
      .pipe(map((data: ApiResult<AccountObject>) => data.response));
  }

  public resetPassword(
    accountId: string,
    code: string,
    password: string
  ): Observable<AccountObject> {
    return this._httpClient
      .get<ApiResult<AccountObject>>(
        `${
          environment.apiUrl
        }account/reset/password?accountId=${encodeURIComponent(
          accountId
        )}&resetCode=${encodeURIComponent(
          code
        )}&newPassword=${encodeURIComponent(password)}`
      )
      .pipe(map((data: ApiResult<AccountObject>) => data.response));
  }

  public updatePassword(
    accountId: string,
    oldPassword: string,
    newPassword: string
  ): Observable<Account> {
    return this._httpClient
      .get<ApiResult<Account>>(
        `${
          environment.apiUrl
        }account/update/password?accountId=${encodeURIComponent(
          accountId
        )}&oldPassword=${encodeURIComponent(
          oldPassword
        )}&newPassword=${encodeURIComponent(newPassword)}`
      )
      .pipe(map((data: ApiResult<Account>) => data.response));
  }

  public getCustomerIp(): Observable<CustomerFootPrintObject> {
    return this._httpClient
      .get<CustomerFootPrintObject>(`https://ipapi.co/json/`)
      .pipe(map((data: CustomerFootPrintObject) => data));
  }

  private _handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code. The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError(error);
  }
}
