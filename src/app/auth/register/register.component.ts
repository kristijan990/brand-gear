import { Component, OnInit, Inject, ɵɵsetComponentScope } from '@angular/core';
import { EmailValidator, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject, takeUntil } from 'rxjs';
import { LookUpObject } from 'src/app/shared/lookup/lookup.model';
import { LookupService } from 'src/app/shared/lookup/lookup.service';
import { ApiResult } from 'src/app/shared/shared.model';
import { SharedService } from 'src/app/shared/shared.service';
import { AccountCreateObject, AccountObject } from '../auth.model';
import { AuthService } from '../auth.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  accountTypeList: Array<LookUpObject> =  new Array<LookUpObject>();
  editFormGroup: FormGroup;
  showSpinner: Boolean = false;
  isEmailValid: Boolean = true;
  fieldTextType: Boolean = true;
  private _unsubscribeAll: Subject<void>  = new Subject<void>();


  constructor(
              private _lookupService: LookupService,
              public _formBuilder: FormBuilder,
              private _authService: AuthService,
              private _sharedService: SharedService,
              private _router: Router
  ) {

  }

  ngOnInit(): void {

    this.getObjectType();
    this.initContactFormGroup();
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.complete();
    this._unsubscribeAll.next();
  }

  initContactFormGroup() {

     return this.editFormGroup = this._formBuilder.group({
       firstName: ["", Validators.required],
       lastName: ["", Validators.required],
       email: ["", [Validators.required, Validators.email]],
       accountType: ["", Validators.required],
       password: ["", Validators.required],



    })
  }

  public createAccount() {
    this.showSpinner = true;
    let accountObject = new AccountCreateObject();
    accountObject.firstName = this.editFormGroup.get('firstName').value;
    accountObject.lastName = this.editFormGroup.get('lastName').value;
    accountObject.email = this.editFormGroup.get('email').value;
    accountObject.accountType = this.editFormGroup.get('accountType').value;
    accountObject.password = this.editFormGroup.get('password').value;


    this._authService.registerCustomer(accountObject).pipe(takeUntil(this._unsubscribeAll)).subscribe(
      (data: AccountObject) => {
        Swal.fire({
          icon: 'info',
          title: 'Your registration was successful. Please check your email for verification!',
          showConfirmButton: false,
          timer: 2000,
          willClose: () => {
            this._router.navigate(['auth/login']);

          },

        });
        this.showSpinner = false;


      })


  }


   getObjectType(){

    this._lookupService.lookUpObjectList().pipe(takeUntil(this._unsubscribeAll)).subscribe((data: Array<LookUpObject>) =>{
      console.log('Lookup =>', data)
      this.accountTypeList = data;
    })
  }

  checkEmail(event: any) {
    let email = this.editFormGroup.getRawValue().email;
    console.log('email ', email);
    let invalidEmail = document.getElementById('alert-message');
    this._authService.checkEmailStatus(email).pipe(takeUntil(this._unsubscribeAll)).subscribe(
      (data: Boolean) => {
        if(data == true) {
          this.isEmailValid = true;
          invalidEmail.style.display = "none";
          console.log('emailValid')
        } else if(data == false) {
          this.isEmailValid = false;
          invalidEmail.style.display = "block";
          console.log('emailInvalid')
        }
      }
    )


  }

  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }



}
