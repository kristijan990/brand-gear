
export class AccountCreateObject {
  constructor(
    public firstName: string | null = null,
    public lastName: string | null = null,
    public email: string | null = null,
    public password: string | null = null,
    public accountType: string | null = null
  ) {}
}


export class AccountObject {
  constructor(
    public accountId: string | null = null,
    public firstName: string | null = null,
    public lastName: string | null = null,
    public email: string | null = null,
    public accountType: string | null = null,
    public accountTypeName: string | null = null,
    public accountStatus: string | null = null,
    public accountStatusName: string | null = null,
    public accountDetailsStatus: string | null = null,
    public accountDetailsStatusName: string | null = null,
    public gender: string | null = null,
    public genderName: string | null = null,
    public dateCreated: string | null = null,
    public phoneNumber: string | null = null,
    public guid: string | null = null,
    public profilePicture: string | null = null,
    public location: string | null = null

  ) {}
}

export class AccessPages {
  constructor(
      public pageName?: string,
      public pageUrl?: string,
      public icon?: string
  ){}
}

export class Account {
  constructor(
      public account?: AccountObject,
      public accesiblePages?: AccessPages[],
      public token?: string
  ){}
}

export class AccountLoginObject {
  constructor(
    public email: string | null = null,
    public password: string | null = null
  ) {}
}
  export class ForgotMail {
    constructor(
      public email: string | null = null,
    ) {}
  }


  export class CustomerFootPrintObject {
    constructor(
      public ip: string | null = null,
      public version: string | null = null,
      public city: string | null = null,
      public region: string | null = null,
      public region_code: string | null = null,
      public country: string | null = null,
      public country_name: string | null = null,
      public country_code: string | null = null,
      public country_code_iso3: string | null = null,
      public country_capital: string | null = null,
      public country_tld: string | null = null,
      public continent_code: string | null = null,
      public in_eu: string | null = null,
      public postal: string | null = null,
      public latitude: string | null = null,
      public longitude: string | null = null,
      public timezone: string | null = null,
      public utc_offset: string | null = null,
      public country_calling_code: string | null = null,
      public currency: string | null = null,
      public currency_name: string | null = null,
      public languages: string | null = null,
      public country_area: string | null = null,
      public country_population: string | null = null,
      public asn: string | null = null,
      public org: string | null = null,
    ) {}
  }

