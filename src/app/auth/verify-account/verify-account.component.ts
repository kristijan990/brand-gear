import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { Subject, takeUntil } from 'rxjs';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-verify-account',
  templateUrl: './verify-account.component.html',
  styleUrls: ['./verify-account.component.scss']
})
export class VerifyAccountComponent implements OnInit, OnDestroy {

  private _unsubscribeAll: Subject<void>  = new Subject<void>();

  constructor(private _activeRoute: ActivatedRoute,
             private  _authService: AuthService,
             private _router: Router) { }

  ngOnInit(): void {
    this.getParamGuid();
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.complete();
    this._unsubscribeAll.next();
  }

  public getParamGuid() {


    this._activeRoute.params.pipe(takeUntil(this._unsubscribeAll)).subscribe(
      (routeParams: Params) => {

        let guid: string = routeParams['guid'];

        this._authService.verifyAccount(guid).pipe(takeUntil(this._unsubscribeAll)).subscribe(
         (data: Boolean)  => {
          Swal.fire({
            icon: 'success',
            title: 'Your verification has been completed successfully!',
            showConfirmButton: false,
            timer: 2000,
            willClose: () => {
            this._router.navigate(['auth/login']);
            },
          });

          },
          () =>
          {
            Swal.fire({
              icon: 'error',
              title: 'An error occurred. Please try again!',
              showConfirmButton: false,
              timer: 2000,
              willClose: () => {

              },
            });

          }
        );



          ;
        }


    )}


}
