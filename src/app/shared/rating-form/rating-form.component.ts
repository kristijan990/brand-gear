import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, Params } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { CampaignDetailsComponent } from 'src/app/campaign/campaign-details/campaign-details.component';
import {
  CampaignsAplicantObject,
  OpenRatingDialogDataObject,
} from 'src/app/campaign/campaign.model';
import { CampaignService } from 'src/app/campaign/campaign.service';
import Swal from 'sweetalert2';
import { CreateRating, RatingDetails, RatingObject } from '../shared.model';
import { SharedModule } from '../shared.module';
import { SharedService } from '../shared.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CampaignAplicationDetailsTVComponent } from 'src/app/campaign/campaign-details/campaign-aplication-details-t-v/campaign-aplication-details-t-v.component';

@Component({
  selector: 'app-rating-form',
  templateUrl: './rating-form.component.html',
  styleUrls: ['./rating-form.component.scss'],
})
export class RatingFormComponent implements OnInit {
  public form: FormGroup;
  is_company: Boolean = false;
  activeGuid: string = null;
  private _unsubscribeAll: Subject<void> = new Subject<void>();
  ratingObject: RatingObject = new RatingObject();
  is_editing: Boolean = false;

  constructor(
    private fb: FormBuilder,
    private _sharedService: SharedService,
    private _authService: AuthService,
    private _campaignService: CampaignService,
    private _activatedRoute: ActivatedRoute,
    public MatDialogRef: MatDialogRef<RatingFormComponent>,
    @Inject(MAT_DIALOG_DATA) private data: OpenRatingDialogDataObject
  ) {
    this.form = this.fb.group({
      rating: [0, Validators.required],
      leaveComment: [null, Validators.required],
    });
  }

  ngOnInit(): void {
    this._activatedRoute.params
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((params: Params) => {
        if (params['guid']) {
          this.activeGuid = params['guid'];
        }
      });

    this.getAccountType();
    this.is_editing = false;
    if (this.ratingObject.feedbackId != null) {
      this.is_editing = true;
    } else {
      this.is_editing = false;
    }
  }

  getAccountType() {
    this._sharedService.is_comapny
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((data: Boolean) => {
        this.is_company = data;
        console.log('is_company ----->', this.is_company);
      });
  }

  saveRating() {
    let rating = new CreateRating();
    // let newCampaignObject = new CampaignsDetails();

    // rating.campaignApplicationId = this.data.applicant.campaignApplicationId;

    if (this.is_company) {
      rating.accountId = this.data.applicant.applicantId;
      rating.companyRating = this.form.get('rating').value;
      rating.companyComment = this.form.get('leaveComment').value;
      rating.companyCommentDate = this.ratingObject.companyCommentDate;
      rating.customerComment = this.ratingObject.customerComment;
      rating.customerRating = this.ratingObject.customerRating;
      rating.customerCommentDate = this.ratingObject.customerCommentDate;
      rating.campaignId = this.data.campaign_id;
    } else if (!this.is_company) {
      rating.accountId =
        this._authService.currentActiveAccount.account.accountId;
      rating.customerRating = this.form.get('rating').value;
      rating.customerComment = this.form.get('leaveComment').value;
      rating.customerCommentDate = this.ratingObject.customerCommentDate;
      rating.companyRating = this.ratingObject.companyRating;
      rating.companyComment = this.ratingObject.companyComment;
      rating.companyCommentDate = this.ratingObject.companyCommentDate;
      rating.campaignId = this.data.campaign_id;
    }

    this._campaignService
      .createRating(rating)
      .pipe()
      .subscribe((data: any) => {
        console.log('create rating data -> ', data);
        if (data) {
          Swal.fire({
            icon: 'success',
            title: 'Review shared !',
            showConfirmButton: false,
            timer: 2000,
            willClose: () => {
              this.MatDialogRef.close(true);
            },
          });
        }
      });
  }
}
