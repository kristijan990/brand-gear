import { Component, HostListener, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter, Subject, takeUntil } from 'rxjs';
import { Account, AccountLoginObject, AccountObject } from 'src/app/auth/auth.model';
import { AuthService } from 'src/app/auth/auth.service';
import { SharedService } from '../shared.service';
import {BreakpointObserver, BreakpointState, LayoutModule, MediaMatcher} from '@angular/cdk/layout';
import { Breakpoints } from '@angular/cdk/layout';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  isLogIn: Boolean = false;
  showMobileMenu: Boolean = false;
  showDropDown: Boolean = false;
  isScrolled: Boolean = false;
  currentActiveAccount: string = null;
  showSpinner: Boolean = false;
  is_mobile: Boolean = false;
  private _unsubscribeAll: Subject<void> = new Subject<void>();



  constructor(
    private _authService: AuthService,
    private _activRoute: ActivatedRoute,
    public _router: Router,
    public _sharedService: SharedService,
    mediaMatcher: MediaMatcher,
    private observer: BreakpointObserver
  ) {

  }

  ngOnInit(): void {
    this.observer
    .observe(['(max-width: 767px)'])
    .subscribe((state: BreakpointState) => {
      if (state.matches) {
        this.is_mobile = true;
      } else {
         this.is_mobile = false;
      }
    });

    this.getAccountStatus();
    this. getAccountInfo();
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  logOut() {
    this._sharedService.is_loaded.next(false);
  setTimeout(()=>{
    this._authService.logOut();
    this._sharedService.is_loaded.next(true);
}, 500);
  }

  getAccountInfo() {
    this._authService.currentActiveAccountSubject
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((data: Account) => {
        if (data.account && data.account.firstName)
          this.currentActiveAccount = data.account.firstName;
      });
  }

  getAccountStatus() {

    this._authService.currentActiveAccountSubject.pipe(takeUntil(this._unsubscribeAll)).subscribe(
      (data: Account) => {
        // let firstName = data.account.firstName;
        if (data.account && data.account.firstName) {
          this.isLogIn = true;
        } else {
          this.isLogIn = false;
        }
      });
  }

checkIfIsLoaded() {
  this._sharedService.is_loaded.pipe(takeUntil(this._unsubscribeAll)).subscribe(
    (data: Boolean) => {
      this.isLogIn = data;
    }

  )
}

  @HostListener('window:scroll')
  scrollEvent() {
    if (this._router.url.startsWith('/')) {
      window.pageYOffset >= 80
        ? (this.isScrolled = true)
        : (this.isScrolled = false);
    }
  }

}
