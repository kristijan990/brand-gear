import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { BehaviorSubject, map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import {
  ApiResult,
  CreateProof,
  ProofDetails,
  UpdateProof,
} from './shared.model';

@Injectable({
  providedIn: 'root',
})
export class SharedService {
  is_login: BehaviorSubject<Boolean> = new BehaviorSubject<Boolean>(false);
  is_account_competed: BehaviorSubject<Boolean> = new BehaviorSubject<Boolean>(
    false
  );
  is_comapny: BehaviorSubject<Boolean> = new BehaviorSubject<Boolean>(false);
  is_loaded: BehaviorSubject<Boolean> = new BehaviorSubject<Boolean>(true);
  is_saving_results: BehaviorSubject<Boolean> = new BehaviorSubject<Boolean>(
    false
  );
  wrongPassword: BehaviorSubject<Boolean> = new BehaviorSubject<Boolean>(false);
  company_comment: BehaviorSubject<Boolean> = new BehaviorSubject<Boolean>(
    false
  );
  customer_comment: BehaviorSubject<Boolean> = new BehaviorSubject<Boolean>(
    false
  );
  aplicants_table: BehaviorSubject<Boolean> = new BehaviorSubject<Boolean>(
    false
  );
  participants_table: BehaviorSubject<Boolean> = new BehaviorSubject<Boolean>(
    false
  );

  private _https_header: HttpHeaders = this.tokenHeader();
  token = this._cookieService.get('BranGearToken');

  constructor(
    private _cookieService: CookieService,
    private _httpClient: HttpClient
  ) {}

  tokenHeader() {
    let $tokenCookie = this._cookieService.get('BranGearToken');
    if (typeof $tokenCookie !== 'undefined' && $tokenCookie !== '') {
      let header = new HttpHeaders({
        accept: 'application/json',
        Authorization: $tokenCookie,
      });
      return header;
    }

    return new HttpHeaders();
  }

  createProof(createProof: CreateProof): Observable<ProofDetails> {
    return this._httpClient
      .post<ApiResult<ProofDetails>>(
        `${environment.apiUrl}proof/create`,
        createProof,
        { headers: this._https_header }
      )
      .pipe(map((data: ApiResult<ProofDetails>) => data.response));
  }

  updateProof(updateProof: UpdateProof): Observable<ProofDetails> {
    return this._httpClient
      .post<ApiResult<ProofDetails>>(
        `${environment.apiUrl}proof/update`,
        updateProof,
        { headers: this._https_header }
      )
      .pipe(map((data: ApiResult<ProofDetails>) => data.response));
  }
}
