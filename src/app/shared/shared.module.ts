import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RatingDetailsComponent } from './rating-details/rating-details.component';
import { ProofFormComponent } from './proof-form/proof-form.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { NgxStarRatingModule } from 'ngx-star-rating';
import { RatingFormComponent } from './rating-form/rating-form.component';

@NgModule({
  declarations: [
    RatingDetailsComponent,
    ProofFormComponent,
    RatingFormComponent,
  ],
  imports: [
    CommonModule,
    NgxStarRatingModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatIconModule,
    MatProgressSpinnerModule,
  ],

  exports: [RatingDetailsComponent],
})
export class SharedModule {}
