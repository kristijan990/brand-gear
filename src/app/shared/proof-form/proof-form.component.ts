import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject, takeUntil } from 'rxjs';
import { CustomerFootPrintObject } from 'src/app/auth/auth.model';
import { AuthService } from 'src/app/auth/auth.service';
import { CampaignCustomerListResponce, CampaignsAplicantObject, CampaignsDetails, FilterDataTable, ProofVideos } from 'src/app/campaign/campaign.model';
import { CampaignService } from 'src/app/campaign/campaign.service';
import { UserService } from 'src/app/user/user.service';
import Swal from 'sweetalert2';
import { CreateProof, DataTableFilter, ProofDetails, ProofImage, ProofVideo, UpdateProof } from '../shared.model';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-proof-form',
  templateUrl: './proof-form.component.html',
  styleUrls: ['./proof-form.component.scss']
})
export class ProofFormComponent implements OnInit {
  name = 'Dynamic Add Fields';
  values = [];
  private _unsubscribeAll: Subject<void> = new Subject<void>();
  proofFormGroup: FormGroup;
  proofObject: ProofDetails = new ProofDetails();
  footprintModel: ProofDetails = new ProofDetails();
  filter: FilterDataTable = new FilterDataTable();
  campaignAplicantsList: Array<CampaignsAplicantObject> = Array<CampaignsAplicantObject>();

  lat: Number;
  lng: Number;
  showSpinner: boolean = false;
  has_picture: Boolean = false;
  has_video: Boolean = false;
  is_editing: boolean = false;
  is_location_allowed:  Boolean = false;

  constructor(
              @Inject(MAT_DIALOG_DATA) private data: CampaignsDetails,
              public _dialogRef: MatDialogRef<ProofFormComponent>,
              public _formBuilder: FormBuilder,
              private _sharedService: SharedService,
              private _authService: AuthService,
              private _userService: UserService,
              private _campaignService: CampaignService
  ) {


    console.log('data -> ', data);

    this.initProofFormGroup();
    //  this.addnewPhoto();
   }

   removevalue(i){
    this.values.splice(i,1);
  }

  addvalue(){
    this.values.push({value: ""});
  }

  ngOnInit(): void {
    this._getLocation();
    this.is_editing = false;
    if (this.proofObject.proofId != null) {
      this.is_editing = true;
    } else {
      this.is_editing = false;
    }


  }

  initProofFormGroup() {
    return (this.proofFormGroup = this._formBuilder.group({
      campaignApplicationId: [this.proofObject.campaignApplicationId, Validators.required],
      remoteIp: [this.proofObject.remoteIp, Validators.required],
      ipAddress: [this.proofObject.ipAddress, Validators.required],
      country: [this.proofObject.country, Validators.required],
      region: [this.proofObject.region, Validators.required],
      code: [this.proofObject.code, Validators.required],
      locationCode: [this.proofObject.locationCode, Validators.required],
      city: [this.proofObject.city, Validators.required],
      fqcn: [this.proofObject.fqcn, Validators.required],
      latitude: [this.proofObject.latitude, Validators.required],
      longitude: [this.proofObject.longitude, Validators.required],
      capital: [this.proofObject.capital, Validators.required],
      timezone: [this.proofObject.timezone, Validators.required],
      currency: [this.proofObject.currency, Validators.required],
      osName: [this.proofObject.osName, Validators.required],
      osVersion: [this.proofObject.osVersion, Validators.required],
      browserName: [this.proofObject.browserName, Validators.required],
      browserVersion: [this.proofObject.browserVersion, Validators.required],
      userAgent: [this.proofObject.userAgent, Validators.required],
      appVersion: [this.proofObject.appVersion, Validators.required],
      plarform: [this.proofObject.plarform, Validators.required],
      vendor: [this.proofObject.vendor, Validators.required],
      images: [this.proofObject.images ? this.proofObject.images : new Array<ProofImage>(), Validators.required],
      videos: [this.proofObject.videos ? this.proofObject.videos : new Array<ProofVideo>(), Validators.required],
      merchingImages: this._formBuilder.array([]),
      merchingVideos: this._formBuilder.array([])

    }));
  }

  //// Function for adding uploaded images in form array

  addnewPhoto(image: ProofImage){
    console.log('image', image)

    const imageForm: FormGroup = this._formBuilder.group({
        images: [image.imageUrl, Validators.required],
    });

    (<FormArray>this.proofFormGroup.get('merchingImages')).push(imageForm);
    console.log('this.proofFormGroup.get(merchingImages) -> ', this.proofFormGroup.get('merchingImages'));
    debugger;
    //this.merching.push(merchForm)
  }

  //// Function for adding uploaded videos in form array

  addnewVideo(video: ProofVideo){
    console.log('video', video)

    const videoForm: FormGroup = this._formBuilder.group({
        videos: [video.videoUrl, Validators.required],
        // videos: ['', Validators.required],
    });
    console.log(this.proofFormGroup.get('merchingVideos'));
    (<FormArray>this.proofFormGroup.get('merchingVideos')).push(videoForm);
    console.log(this.proofFormGroup);
    console.log(<FormArray>this.proofFormGroup.get('merchingVideos'));
    //this.merching.push(merchForm)
  }

  removeImage(i: number) {
    (<FormArray>this.proofFormGroup.get('merchingImages')).removeAt(i);
    let image_list: Array<ProofImage> = this.proofFormGroup.get('images').value as Array<ProofImage>;
    image_list.splice(i, 1);
    this.proofFormGroup.get('images').setValue(image_list);
  }

  removeVideo(i: number) {
    (<FormArray>this.proofFormGroup.get('merchingImages')).removeAt(i);
    let video_list: Array<ProofVideo> = this.proofFormGroup.get('videos').value as Array<ProofVideo>;
    video_list.splice(i, 1);
    this.proofFormGroup.get('videos').setValue(video_list);
  }



  patchValue() {
    this.proofFormGroup.patchValue({
      campaignApplicationId: this.proofObject.campaignApplicationId,

      images: this.proofObject.images,
      videos: this.proofObject.videos,

    });
  }

  private _module: any = {
    options: [],
    header: [navigator.platform, navigator.userAgent, navigator.appVersion, navigator.vendor],
    dataos: [
      { name: 'Windows Phone', value: 'Windows Phone', version: 'OS' },
      { name: 'Windows', value: 'Win', version: 'NT' },
      { name: 'iPhone', value: 'iPhone', version: 'OS' },
      { name: 'iPad', value: 'iPad', version: 'OS' },
      { name: 'Kindle', value: 'Silk', version: 'Silk' },
      { name: 'Android', value: 'Android', version: 'Android' },
      { name: 'PlayBook', value: 'PlayBook', version: 'OS' },
      { name: 'BlackBerry', value: 'BlackBerry', version: '/' },
      { name: 'Macintosh', value: 'Mac', version: 'OS X' },
      { name: 'Linux', value: 'Linux', version: 'rv' },
      { name: 'Palm', value: 'Palm', version: 'PalmOS' }
    ],
    databrowser: [
      { name: 'Chrome', value: 'Chrome', version: 'Chrome' },
      { name: 'Firefox', value: 'Firefox', version: 'Firefox' },
      { name: 'Safari', value: 'Safari', version: 'Version' },
      { name: 'Internet Explorer', value: 'MSIE', version: 'MSIE' },
      { name: 'Opera', value: 'Opera', version: 'Opera' },
      { name: 'BlackBerry', value: 'CLDC', version: 'CLDC' },
      { name: 'Mozilla', value: 'Mozilla', version: 'Mozilla' }
    ],
    init: function () {
      var agent = this.header.join(' '),
        os = this.matchItem(agent, this.dataos),
        browser = this.matchItem(agent, this.databrowser);

      return { os: os, browser: browser };
    },
    matchItem: function (string, data) {
      var i = 0,
        j = 0,
        html = '',
        regex,
        regexv,
        match,
        matches,
        version;

      for (i = 0; i < data.length; i += 1) {
        regex = new RegExp(data[i].value, 'i');
        match = regex.test(string);
        if (match) {
          regexv = new RegExp(data[i].version + '[- /:;]([\\d._]+)', 'i');
          matches = string.match(regexv);
          version = '';
          if (matches) { if (matches[1]) { matches = matches[1]; } }
          if (matches) {
            matches = matches.split(/[._]+/);
            for (j = 0; j < matches.length; j += 1) {
              if (j === 0) {
                version += matches[j] + '.';
              } else {
                version += matches[j];
              }
            }
          } else {
            version = '0';
          }
          return {
            name: data[i].name,
            version: parseFloat(version)
          };
        }
      }
      return { name: 'unknown', version: 0 };
    }
  };

  //////// Function for getting  user location

  private _getLocation() {
    console.log('navigator ->', navigator)
    console.log('navigator.geolocation ->', navigator.geolocation)
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position: GeolocationPosition) => {
          // if (position) {
            console.log("Latitude: " + position.coords.latitude);
            console.log("Longitude: " + position.coords.longitude);
            this.lat = position.coords.latitude;
            this.lng = position.coords.longitude;
            this.is_location_allowed = true;
            this._customerFootprint();
          // }
        },
        (error: any) => {
          console.warn('position error -> ',error);
          this.is_location_allowed = false;
          console.log('is allowed --> ', this.is_location_allowed)
        }
      );
    } else {
      alert("Geolocation is not supported by this browser.");
      this.is_location_allowed = false;
      console.log('is allowed --> ', this.is_location_allowed)
    }
  }

  private _customerFootprint() {
    let e = this._module.init();

    this._authService.getCustomerIp().pipe(takeUntil(this._unsubscribeAll)).subscribe(
      (data: CustomerFootPrintObject) => {
        console.log('customer ip -> ', data);
        this.footprintModel.remoteIp = data.ip;
        this.footprintModel.ipAddress = data.ip;
        this.footprintModel.country = data.country_name;
        this.footprintModel.region = data.region;
        this.footprintModel.code = data.region_code;
        this.footprintModel.locationCode = data.postal;
        this.footprintModel.city = data.city;
        this.footprintModel.fqcn = data.org;
        this.footprintModel.latitude = this.lat.toString();
        this.footprintModel.longitude = this.lng.toString();
        this.footprintModel.capital = data.country_capital;
        this.footprintModel.timezone = data.utc_offset;
        this.footprintModel.currency = data.currency;
        this.footprintModel.osName = e.os.name;
        this.footprintModel.osVersion = String(e.os.version);
        this.footprintModel.browserName = e.browser.name;
        this.footprintModel.browserVersion = String(e.browser.version);
        this.footprintModel.userAgent = navigator.userAgent;
        this.footprintModel.appVersion = navigator.appVersion;
        this.footprintModel.plarform = navigator.platform;
        this.footprintModel.vendor = navigator.vendor;
        this.footprintModel.timestamp = data.timezone;


        console.log('footprint --> ', this.footprintModel)
      }
    );
  }

  //////// FUNCTION FOR CREATE/UPDATE PROOF

  updateProof(): void {

    let createProof = new CreateProof();

    createProof.campaignApplicationId = this.data.campaignApplicationId;
    createProof.remoteIp = this.footprintModel.remoteIp;
    createProof.ipAddress = this.footprintModel.ipAddress;
    createProof.country = this.footprintModel.country;
    createProof.region = this.footprintModel.region;
    createProof.code = this.footprintModel.code;
    createProof.locationCode = this.footprintModel.locationCode;
    createProof.city = this.footprintModel.city;
    createProof.fqcn = this.footprintModel.fqcn;
    createProof.latitude = this.footprintModel.latitude;
    createProof.longitude = this.footprintModel.longitude;
    createProof.capital = this.footprintModel.capital;
    createProof.timezone = this.footprintModel.timestamp;
    createProof.currency = this.footprintModel.currency
    createProof.osName = this.footprintModel.osName;
    createProof.osVersion = this.footprintModel.osVersion;
    createProof.browserName = this.footprintModel.browserName
    createProof.browserVersion = this.footprintModel.browserVersion;
    createProof.userAgent = this.footprintModel.userAgent;
    createProof.appVersion = this.footprintModel.appVersion;
    createProof.plarform = this.footprintModel.plarform;
    createProof.vendor = this.footprintModel.vendor;
    createProof.images = this.proofFormGroup.get('images').value;
    createProof.videos = this.proofFormGroup.get('videos').value;

    if(this.is_editing) {
      let updateProof: UpdateProof = new UpdateProof();


      updateProof = createProof;
      updateProof.proofId;

      this._sharedService.updateProof(updateProof).pipe(takeUntil(this._unsubscribeAll)).subscribe(
        (data: ProofDetails) => {

          this._dialogRef.close(true);
        }
      )

    }

    else {
     this.showSpinner = true;
    //  let createProof = new CreateProof();
     console.log('filter',createProof);
     this._sharedService.createProof(createProof)
       .pipe()
       .subscribe((data: ProofDetails) => {
         if (data) {
           Swal.fire({
             icon: 'success',
             title: 'File uploaded!',
             showConfirmButton: false,
             timer: 2000,
             willClose: () => {

               this._dialogRef.close(true);
               this.showSpinner = false;
             },
           });
         }
       });


    }

   }



      ///////// Funtion for uploading images

   uploadImage(event: any): void {
     if(this.is_location_allowed) {
      console.log('file --> ', event);
      if (event.target.files.length > 0) {
        this.showSpinner = true;

        const file: File = (
          (event.target as HTMLInputElement).files as FileList
        )[0];

        let form_data: FormData = new FormData();
        form_data.append('file', file, file.name);
        console.log('file --> ', file);


        this._userService
          .mediaImage(form_data)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(
            (data: string) => {
              Swal.fire({
                icon: 'success',
                text: 'Photo is successfully uploaded!',
                showConfirmButton: false,
                timer: 2000,
                willClose: () => {
                  this.afterUploadImage(data);
                },
              });

            },

            () => {
              Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Something went wrong! Please try again',
                showConfirmButton: false,
                timer: 2000,
                willClose: () => {
                  this.showSpinner = false;
                  event.target.value = null;

                },
              });
            }
          );
      }

     } else {
      Swal.fire({
        icon: 'info',
        title: 'Oops...',
        text: 'Please allow your location first!',
        showConfirmButton: false,
        timer: 2000,
        willClose: () => {
          this.showSpinner = false;
          // event.target.value = null;

        },
      });

     }

  }

  afterUploadImage(data) {
    console.log('photo url --> ', data);

    // this.patientForm.get('profilePicture')?.setValue(data);
    let images: Array<ProofImage> = this.proofFormGroup.get('images')?.value;
    images = images && images.length == 0 ? new Array<ProofImage>() : images;

    let single_image: ProofImage = new ProofImage();
    single_image.imageUrl = data;
    images.push(single_image);
    this.proofFormGroup.get('images')?.setValue(images);
    console.log('proof for group images value -> ', this.proofFormGroup.get('images')?.value)
    console.log(
      'campaignPicture',
      this.proofFormGroup.get('images')
    );

    this.addnewPhoto(single_image);

    this.showSpinner = false;
    this.has_picture = true;
    // this._getLocation();
  }
    ///////// Funtion for uploading videos

  uploadVideo(event: any): void {
    if(this.is_location_allowed) {
      console.log('file --> ', event);
      if (event.target.files.length > 0) {
        this.showSpinner = true;

        const file: File = (
          (event.target as HTMLInputElement).files as FileList
        )[0];

        let form_data: FormData = new FormData();
        form_data.append('file', file, file.name);
        console.log('file --> ', file);


        this._userService
          .mediaVideo(form_data)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(
            (data: string) => {
              Swal.fire({
                icon: 'success',
                text: 'Video is successfully uploaded!',
                showConfirmButton: false,
                timer: 2000,
                willClose: () => {
                  this.afterVideoUpload(data);
                },
              });

            },

            () => {
              Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Something went wrong! Please try again',
                showConfirmButton: false,
                timer: 2000,
                willClose: () => {
                  this.showSpinner = false;
                  event.target.value = null;

                },
              });
            }
          );
      }

    } else {
         Swal.fire({
                icon: 'info',
                title: 'Oops...',
                text: 'Please allow your location first!',
                showConfirmButton: false,
                timer: 2000,
                willClose: () => {
                  this.showSpinner = false;
                  event.target.value = null;

                },
              });

    }


  }

  afterVideoUpload(data){

    console.log('photo url --> ', data);
    let videos: Array<ProofVideo> = this.proofFormGroup.get('videos')?.value;
    videos = videos && videos.length == 0 ? new Array<ProofVideo>() : videos;

    let single_video: ProofVideo = new ProofVideo();
    single_video.videoUrl = data;
    videos.push(single_video);
    this.proofFormGroup.get('videos')?.setValue(videos);
    console.log('proof for group videos value -> ', this.proofFormGroup.get('videos')?.value)
    console.log(
      'campaignPicture',
      this.proofFormGroup.get('videos')
    );

    this.addnewVideo(single_video);

    this.showSpinner = false;
    this.has_video = true;
    // this._getLocation();
  }

}
