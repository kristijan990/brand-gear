import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, map, Observable, retry } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ApiResult } from '../shared.model';
import { LookUpObject } from './lookup.model';

@Injectable({
  providedIn: 'root'
})
export class LookupService {

  accountTypeList : BehaviorSubject<Array<LookUpObject>> =  new BehaviorSubject<Array<LookUpObject>>( Array<LookUpObject>());

  constructor(private _httpClient: HttpClient) { }




  public lookUpObjectList(): Observable<Array<LookUpObject>> {
    return this._httpClient.get<ApiResult<Array<LookUpObject>>>(`${environment.apiUrl}lookup/account/type/list`).pipe(
      map((data: ApiResult<Array<LookUpObject>>) => {
        this.accountTypeList.next(data.response);
        return data.response;
      }));

  }

  public lookUpGenderList(): Observable<Array<LookUpObject>> {
    return this._httpClient.get<ApiResult<Array<LookUpObject>>>(`${environment.apiUrl}lookup/gender/list`).pipe(
      map((data: ApiResult<Array<LookUpObject>>) => {
        this.accountTypeList.next(data.response);
        return data.response;
      }));

  }
}
