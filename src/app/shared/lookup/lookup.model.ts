export class LookUpObject {
  constructor(
   public value: string | null = null,
   public name: string | null = null,
   public description: string | null = null
  ) { }
}

