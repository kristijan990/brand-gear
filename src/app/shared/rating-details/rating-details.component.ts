import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { CampaignService } from 'src/app/campaign/campaign.service';
import { CustomerService } from 'src/app/customer/customer.service';
import Swal from 'sweetalert2';
import { RatingDetails } from '../shared.model';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-rating-details',
  templateUrl: './rating-details.component.html',
  styleUrls: ['./rating-details.component.scss'],
})
export class RatingDetailsComponent implements OnInit {
  @Input() ratingDetails: RatingDetails = new RatingDetails();

  private _unsubscribeAll: Subject<void> = new Subject<void>();

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _customerService: CustomerService
  ) {}
  ngOnInit(): void {
    console.log('rating details --> ', this.ratingDetails);
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.complete();
    this._unsubscribeAll.next();
  }

  // getAplicantProfile(): void {
  //   this._router.navigate([`../customer/details/${this.ratingDetails.campaignApplicationId}`], {relativeTo: this._route })

  // }

  // removeRating(feedbackId: string) {
  //   console.log('feedBack ID --------->', feedbackId);

  //   Swal.fire({
  //     title: 'Are you sure?',
  //     text: "You won't be able to revert this!",
  //     icon: 'warning',
  //     showCancelButton: true,
  //     confirmButtonColor: '#3085d6',
  //     cancelButtonColor: '#d33',
  //     confirmButtonText: 'Yes, delete it!',
  //   }).then((result) => {
  //     if (result.isConfirmed) {
  //       this._customerService
  //         .deleteRating(feedbackId)
  //         .pipe(takeUntil(this._unsubscribeAll))
  //         .subscribe((data: Boolean) => {
  //           if (data) {
  //             Swal.fire('Deleted!', 'Your file has been deleted.', 'success');
  //           }
  //         });
  //     }
  //   });
  // }
}
