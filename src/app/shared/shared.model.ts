export class ApiResult<T> {
  constructor(
    public message: string | null = null,
    public statusCode: string | null = null,
    public response: T
  ) {}
}

export class DataTableFilter {
  constructor(
    public pageNumber: number | null = null,
    public pageSize: number | null = null,
    public sortColumn: string | null = null,
    public sortDirection: string | null = null,
    public companyId: string | null = null
  ) {}
}
export class DataTableResponse<T> {
  constructor(
    public pageNumber: number | null = null,
    public pageSize: number | null = null,
    public totalPages: number | null = null,
    public totalRecords: number | null = null,
    public data: T
  ) {}
}

export class FilterRatingList {
  constructor(
    public pageNumber: number | null = null,
    public pageSize: number | null = null,
    public sortColumn: string | null = null,
    public sortDirection: string | null = null,
    public accountId: string | null = null,
    public campaignId: string | null = null
  ) {}
}

export class RatingObject {
  constructor(
    public feedbackId: string | null = null,
    public campaignApplicationId: string | null = null,
    public customerRating: number | null = null,
    public customerComment: string | null = null,
    public customerCommentDate: string | Date | null = null,
    public companyRating: number | null = null,
    public companyComment: string | null = null,
    public companyCommentDate: string | Date | null = null
  ) {}
}

export class RatingDetails extends RatingObject {
  constructor(
    public companyAvatar: string | null = null,
    public companyName: string | null = null,
    public customerName: string | null = null,
    public customerAvatar: string | null = null
  ) {
    super();
  }
}

export class CreateRating extends RatingObject {
  constructor(
    public campaignId: string | null = null,
    public accountId: string | null = null
  ) {
    super();
  }
}

export class ProofDetails {
  constructor(
    public proofId: string | null = null,
    public campaignApplicationId: string | null = null,
    public remoteIp: string | null = null,
    public ipAddress: string | null = null,
    public certainty: string | null = null,
    public internet: string | null = null,
    public country: string | null = null,
    public regionLocationCode: string | null = null,
    public region: string | null = null,
    public code: string | null = null,
    public locationCode: string | null = null,
    public city: string | null = null,
    public cityId: string | null = null,
    public fqcn: string | null = null,
    public latitude: string | null = null,
    public longitude: string | null = null,
    public capital: string | null = null,
    public timezone: string | null = null,
    public nationalitySingular: string | null = null,
    public population: string | null = null,
    public nationalityPlural: string | null = null,
    public mapReference: string | null = null,
    public currency: string | null = null,
    public currencyCode: string | null = null,
    public title: string | null = null,
    public osName: string | null = null,
    public osVersion: string | null = null,
    public browserName: string | null = null,
    public browserVersion: string | null = null,
    public userAgent: string | null = null,
    public appVersion: string | null = null,
    public plarform: string | null = null,
    public vendor: string | null = null,
    public images: Array<ProofImage> = new Array<ProofImage>(),
    public videos: Array<ProofVideo> = new Array<ProofVideo>(),
    public timestamp: string | null = null
  ) {}
}

export class ProofImage {
  constructor(
    public proofId: number | null = null,
    public imageUrl: string | null = null
  ) {}
}

export class CreateProofImage {
  constructor(public imageUrl: string | null = null) {}
}

export class UpdateProofImage extends CreateProofImage {
  constructor(
    public proofImageId: number | null = null,
    public proofId: number | null = null
  ) {
    super();
  }
}

export class ProofVideo {
  constructor(
    public proofId: number | null = null,
    public videoUrl: string | null = null
  ) {}
}

export class CreateProofVideo {
  constructor(public videoUrl: string | null = null) {}
}

export class UpdateProofVideo extends CreateProofVideo {
  constructor(
    public proofVideoId: number | null = null,
    public proofId: number | null = null
  ) {
    super();
  }
}

export class CreateProof {
  constructor(
    public campaignApplicationId: string | null = null,
    public remoteIp: string | null = null,
    public ipAddress: string | null = null,
    public certainty: string | null = null,
    public internet: string | null = null,
    public country: string | null = null,
    public regionLocationCode: string | null = null,
    public region: string | null = null,
    public code: string | null = null,
    public locationCode: string | null = null,
    public city: string | null = null,
    public cityId: string | null = null,
    public fqcn: string | null = null,
    public latitude: string | null = null,
    public longitude: string | null = null,
    public capital: string | null = null,
    public timezone: string | null = null,
    public nationalitySingular: string | null = null,
    public population: string | null = null,
    public nationalityPlural: string | null = null,
    public mapReference: string | null = null,
    public currency: string | null = null,
    public currencyCode: string | null = null,
    public title: string | null = null,
    public osName: string | null = null,
    public osVersion: string | null = null,
    public browserName: string | null = null,
    public browserVersion: string | null = null,
    public userAgent: string | null = null,
    public appVersion: string | null = null,
    public plarform: string | null = null,
    public vendor: string | null = null,
    public images: ProofImage | null = null,
    public videos: ProofVideo | null = null
  ) {}
}

export class UpdateProof extends CreateProof {
  constructor(public proofId?: string) {
    super();
  }
}
