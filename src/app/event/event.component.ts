import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { EventFormComponent } from './event-form/event-form.component';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit {

  constructor(
              public dialog: MatDialog
  ) { }

  ngOnInit(): void {
  }

  // function for opening create/edit Event

  addNewEvent() {
    const dialogRef = this.dialog.open(EventFormComponent, {
      data: {
        // data: list,
      },
    });
    dialogRef.afterClosed().subscribe((response) => {
      if (response) {
        // this.getCampaignList();
      }
    });

  }

}
