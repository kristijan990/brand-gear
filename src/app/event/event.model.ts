export class CreateEvent {
  constructor(
    public eventName?: string,
    public eventStartDate?: string,
    public eventEndDate?: string,
    public eventLocation?: string,
    public eventDescription?: string,
    public eventStatus?: string,
    public eventPicture?: string,
    public eventStatusName?: string,
  ) {}
}

export class UpdateEvent extends CreateEvent {
  constructor(public eventId: string | null = null) {
    super();
  }
}


export class EventDetails {
  constructor(
    public eventId: string | null = null,
    public eventName: string | null = null,
    public eventStartDate: string | null = null,
    public eventEndDate: string | null = null,
    public eventLocation: string | null = null,
    public eventDescription: string | null = null,
    public eventPicture: string | null = null,
    public eventStatus: string | null = null,
    public eventStatusName: string | null = null,
    public isActive: Boolean | null = null,
    public eventGuid: string | null = null,
    public eventCampaigns: Boolean | null = null,


  ) {}
}

export class EventFilter {
  constructor(
    public pageNumber: number | null = null,
    public pageSize: number | null = null,
    public sortColumn: string | null = null,
    public sortDirection: string | null = null
  ) {}
}

export class ProofFilter extends EventFilter {
  constructor(
   public campaignId: string | null = null,
   public accountId: string | null = null

  )
  {
    super()
  }
}



export class CampaignEventFilter extends EventFilter {
  constructor(
   public eventId: string | null = null
  )
  {
    super()
  }
}
