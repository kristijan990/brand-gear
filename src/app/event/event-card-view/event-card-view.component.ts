import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { CampaignService } from 'src/app/campaign/campaign.service';
import { SharedService } from 'src/app/shared/shared.service';
import { UserService } from 'src/app/user/user.service';

import { EventFormComponent } from '../event-form/event-form.component';
import { EventDetails } from '../event.model';
import { EventService } from '../event.service';
import { Injectable } from '@angular/core';
import { EventCardListComponent } from '../event-card-list/event-card-list.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-event-card-view',
  templateUrl: './event-card-view.component.html',
  styleUrls: ['./event-card-view.component.scss'],
})
export class EventCardViewComponent implements OnInit {
  showSpinner: Boolean = false;
  is_saving_results: boolean = false;
  private _unsubscribeAll: Subject<void> = new Subject<void>();
  @Input() list: EventDetails;
  is_company: Boolean = false;

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _campaignService: CampaignService,
    private _authService: AuthService,
    private _userService: UserService,
    private _sharedService: SharedService,
    private dialog: MatDialog,
    private _eventService: EventService,
    private _eventList: EventCardListComponent
  ) {}

  ngOnInit(): void {
    this.checkAccountType();
  }

  editEvent(list: EventDetails) {
    const dialogRef = this.dialog.open(EventFormComponent, {
      data: list,
    });
    console.log('list  ---->', list);
    dialogRef.afterClosed().subscribe((response) => {
      if (response) {
        Swal.fire({
          icon: 'success',
          title: 'Event successfuly edited',
          showConfirmButton: false,
          timer: 2000,
          willClose: () => {
            this.is_saving_results = false;
            this._eventService.getEventDetails(list.eventGuid);
          },
        });
        this._eventService.getEventDetails(list.eventGuid);
        this._eventList.getEventList();
      }
    });
  }

  deleteEvent(eventId: string) {
    console.log('delete');
    Swal.fire({
      title: 'Are you sure you want to remove this event?',

      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'No',
      confirmButtonText: 'Yes',
    }).then((result) => {
      if (result.isConfirmed) {
        this.is_saving_results = true;
        this._eventList.getEventList();

        this._eventService
          .removeEvent(eventId)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(
            () => {
              Swal.fire({
                icon: 'success',
                title: 'Event was successfuly removed!',
                showConfirmButton: false,
                timer: 2000,
                willClose: () => {
                  this.is_saving_results = false;
                  this._eventList.getEventList();
                },
              });
            },
            () => {
              Swal.fire({
                icon: 'error',
                title: 'Something went wrong, please try again!',
                showConfirmButton: false,
                timer: 2000,
                willClose: () => {
                  this.is_saving_results = false;
                },
              });
            }
          );
      }
    });
  }

  viewEventByGuid() {
    this._router.navigate([`../details/${this.list.eventGuid}`], {
      relativeTo: this._route,
    });
  }

  checkAccountType() {
    this._sharedService.is_comapny
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((data: Boolean) => {
        this.is_company = data;
      });
  }
}
