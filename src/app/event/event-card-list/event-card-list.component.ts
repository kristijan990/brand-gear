import { Component, OnInit } from '@angular/core';
import { NgxMasonryOptions } from 'ngx-masonry';
import { Subject, takeUntil } from 'rxjs';
import { DataTableResponse } from 'src/app/shared/shared.model';
import { EventDetails, EventFilter } from '../event.model';
import { EventService } from '../event.service';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { EventFormComponent } from '../event-form/event-form.component';
import { SharedService } from 'src/app/shared/shared.service';

@Component({
  selector: 'app-event-card-list',
  templateUrl: './event-card-list.component.html',
  styleUrls: ['./event-card-list.component.scss'],
})
@Injectable({ providedIn: 'root' })
export class EventCardListComponent implements OnInit {
  private _unsubscribeAll: Subject<void> = new Subject<void>();
  filter: EventFilter = new EventFilter();
  eventList: EventDetails[];
  // list: EventDetails;
  showSpinner: Boolean = false;
  mansonryOptions: NgxMasonryOptions = {};
  is_company: Boolean = false;

  constructor(
    private _eventService: EventService,
    public dialog: MatDialog,
    private _sharedService: SharedService
  ) {}

  ngOnInit(): void {
    this.showSpinner = true;
    this.getEventList();
    this.checkAccountType();
  }

  getEventList(): void {
    this.filter.pageNumber = 1;
    this.filter.pageSize = 10;
    this.filter.sortDirection = 'asc';
    this.filter.sortColumn = '';

    this._eventService
      .listEvent(this.filter)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((data: DataTableResponse<Array<EventDetails>>) => {
        this.eventList = data.data;

        // this._sharedService.is_saving_results.next(true);
        this.showSpinner = false;
      });
  }

  addNewEvent(list: EventDetails) {
    const dialogRef = this.dialog.open(EventFormComponent, {
      data: {
        data: list,
      },
    });
    dialogRef.afterClosed().subscribe((response) => {
      if (response) {
        // this.getCampaignList();
      }
    });
  }

  checkAccountType() {
    this._sharedService.is_comapny
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((data: Boolean) => {
        this.is_company = data;
      });
  }
}
