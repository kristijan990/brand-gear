import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EventCardListComponent } from './event-card-list/event-card-list.component';
import { EventCardViewComponent } from './event-card-view/event-card-view.component';
import { EventDetailsComponent } from './event-details/event-details.component';
import { EventFormComponent } from './event-form/event-form.component';
import { EventComponent } from './event.component';
import { EventDetails } from './event.model';
import { TestComponent } from './test/test.component';


const eventRoutes: Routes = [
  {
    path: '',
    component: EventComponent,
    children: [
      {
        path: 'form',
        component: EventFormComponent,
      },
      {
        path: 'details/:guid',
        component: EventDetailsComponent
      },
      {
        path: 'list',
        component: EventCardListComponent
      },
      {
        path: 'test',
        component: TestComponent
      },
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(eventRoutes)],
  exports: [RouterModule]
})
export class EventRoutingModule { }
