import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject, takeUntil } from 'rxjs';
import { FilterDataTable } from 'src/app/campaign/campaign.model';
import { CampaignService } from 'src/app/campaign/campaign.service';
import { UserService } from 'src/app/user/user.service';
import Swal from 'sweetalert2';
import { CreateEvent, EventDetails, UpdateEvent } from '../event.model';
import { EventService } from '../event.service';

@Component({
  selector: 'app-event-form',
  templateUrl: './event-form.component.html',
  styleUrls: ['./event-form.component.scss']
})
export class EventFormComponent implements OnInit {
  private _unsubscribeAll: Subject<void> = new Subject<void>();
  eventFormGroup: FormGroup;
  eventObject: EventDetails = new EventDetails();
  showSpinner: boolean = false;
  has_picture: Boolean = true;
  is_editing: boolean = false;
  filter: FilterDataTable = new FilterDataTable();

  constructor(
              @Inject(MAT_DIALOG_DATA) data: any,
              public _dialogRef: MatDialogRef<EventFormComponent>,
              public _formBuilder: FormBuilder,
              private _eventServis: EventService,
              private _userService: UserService,
              private _campaignService: CampaignService
  ) {

    // this.is_editing = this.eventObject.eventId ? true : false;
    // this.is_editing = true;

    this.initContactFormGroup();

    this.eventObject = data ? data : new EventDetails();
    if (this.eventObject && this.eventObject.eventId) {
      this.patchValue();
    }
   }

  ngOnInit(): void {
    //// CHECK IF EVENT IS EXISTING
    this.is_editing = false;
    if (this.eventObject.eventId != null) {
      this.is_editing = true;
    } else {
      this.is_editing = false;
    }

    if(this.is_editing) {
      this.has_picture = false;
    }
    else {
      this.has_picture = true;
    }
  }

  initContactFormGroup() {
    return (this.eventFormGroup = this._formBuilder.group({
      eventId: [this.eventObject.eventId, Validators.required],
      eventName: [this.eventObject.eventName, Validators.required],
      eventStartDate: [this.eventObject.eventStartDate, Validators.required],
      eventEndDate: [this.eventObject.eventEndDate, Validators.required],
      eventLocation: [this.eventObject.eventLocation, Validators.required],
      eventDescription: [this.eventObject.eventDescription, Validators.required],
      eventPicture: [this.eventObject.eventPicture, Validators.required],
      eventStatus: [this.eventObject.eventStatus, Validators.required],
      eventStatusName: [this.eventObject.eventStatusName, Validators.required],
      isActive: [this.eventObject.isActive, Validators.required],
      eventGuid: [this.eventObject.eventGuid, Validators.required],
      eventCampaigns: [this.eventObject.eventCampaigns, Validators.required],

    }));
  }

  //////// FUNCTION FOR CREATE/UPDATE EVENT

  updateEvent(): void {

   if(this.is_editing) {

     let updateEvent = new UpdateEvent;
     let newEvenObject = new EventDetails;

     updateEvent.eventName = this.eventFormGroup.get('eventName').value;
     updateEvent.eventPicture = this.eventFormGroup.get('eventPicture').value;
     updateEvent.eventDescription = this.eventFormGroup.get('eventDescription').value;
     updateEvent.eventStartDate = this.eventFormGroup.get('eventStartDate').value;
     updateEvent.eventEndDate = this.eventFormGroup.get('eventEndDate').value;
     updateEvent.eventLocation = this.eventFormGroup.get('eventLocation').value;
     updateEvent.eventId = this.eventObject.eventId;

     this._eventServis.updateEvent(updateEvent).pipe(takeUntil(this._unsubscribeAll)).subscribe(
       (data: EventDetails) => {
         newEvenObject = data;
         this._dialogRef.close(true);
       }
     )




   }

   else {
    this.showSpinner = true;
    let createEvent = new CreateEvent();

    createEvent.eventName = this.eventFormGroup.get('eventName').value;
    createEvent.eventPicture = this.eventFormGroup.get('eventPicture').value;
    createEvent.eventDescription = this.eventFormGroup.get('eventDescription').value;
    createEvent.eventStartDate = this.eventFormGroup.get('eventStartDate').value;
    createEvent.eventEndDate = this.eventFormGroup.get('eventEndDate').value;
    createEvent.eventLocation = this.eventFormGroup.get('eventLocation').value;



    this._eventServis.createEvent(createEvent)
      .pipe()
      .subscribe((data: EventDetails) => {
        if (data) {
          Swal.fire({
            icon: 'success',
            title: 'Event Created !',
            showConfirmButton: false,
            timer: 2000,
            willClose: () => {

              this._dialogRef.close(true);
              this.showSpinner = false;
            },
          });
        }
      });


   }

  }

  /// FUNCTION FOR PATCHING VALUE

  patchValue() {
    this.eventFormGroup.patchValue({
      eventName: this.eventObject.eventName,
      eventPicture: this.eventObject.eventPicture,
      eventLocation: this.eventObject.eventLocation,
      eventStartDate: this.eventObject.eventStartDate,
      eventEndDate: this.eventObject.eventEndDate,
      eventDescription: this.eventObject.eventDescription,

    });
  }

  //// FUNCTION FOR UPLOAD IMAGE

  uploadImage(event: any): void {

    console.log('file --> ', event);
    if (event.target.files.length > 0) {
      this.showSpinner = true;

      const file: File = (
        (event.target as HTMLInputElement).files as FileList
      )[0];

      let form_data: FormData = new FormData();
      form_data.append('file', file, file.name);
      console.log('file --> ', file);


      this._userService
        .mediaImage(form_data)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(
          (data: string) => {
            Swal.fire({
              icon: 'success',
              text: 'Photo is successfully uploaded!',
              showConfirmButton: false,
              timer: 2000,
              willClose: () => {
                console.log('photo url --> ', data);
                // this.patientForm.get('profilePicture')?.setValue(data);
                this.eventFormGroup.get('eventPicture')?.setValue(data);
                console.log(
                  'campaignPicture',
                  this.eventFormGroup.get('eventPicture')
                );
                this.showSpinner = false;
                event.target.value = null;
                this.has_picture = false;

              },
            });
          },
          () => {
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Something went wrong! Please try again',
              showConfirmButton: false,
              timer: 2000,
              willClose: () => {

                event.target.value = null;

              },
            });
          }
        );
    }
  }




}
