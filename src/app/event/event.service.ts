
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Observable, map } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthService } from '../auth/auth.service';
import { CampaignsDetails } from '../campaign/campaign.model';
import {
  ApiResult,
  DataTableFilter,
  DataTableResponse,
} from '../shared/shared.model';
import {
  CampaignEventFilter,
  CreateEvent,
  EventDetails,
  EventFilter,
  ProofFilter,
  UpdateEvent,
} from './event.model';

@Injectable({
  providedIn: 'root',
})
export class EventService {
  private _https_header: HttpHeaders = this._authService.tokenHeader();
  token = this._cookieService.get('BranGearToken');

  constructor(
    private _httpClient: HttpClient,
    private _cookieService: CookieService,
    private _authService: AuthService
  ) {}

  createEvent(createEvent: CreateEvent): Observable<EventDetails> {
     return this._httpClient.post<ApiResult<EventDetails>>(`${environment.apiUrl}event/create`, createEvent,{ headers: this._https_header }).pipe(
       map((data: ApiResult<EventDetails>) => data.response));
  }

  updateEvent(updateEvent: UpdateEvent): Observable<EventDetails> {
    return this._httpClient.post<ApiResult<EventDetails>>(`${environment.apiUrl}event/update`, updateEvent, { headers: this._https_header }).pipe(
      map((data: ApiResult<EventDetails>) => data.response));
  }

  getEventDetails(eventGuid: string): Observable<EventDetails> {
    return this._httpClient.get<ApiResult<EventDetails>>(`${ environment.apiUrl}event/details/by/guid?eventGuid=${encodeURIComponent(eventGuid)}`, { headers: this._https_header }).pipe(
      map((data: ApiResult<EventDetails>) => data.response));
  }

  listEvent(filter: EventFilter): Observable<DataTableResponse<Array<EventDetails>>> {
    return this._httpClient
      .post<ApiResult<DataTableResponse<Array<EventDetails>>>>(`${environment.apiUrl}event/list`, filter, { headers: this._https_header }).pipe(
         map( (data: ApiResult<DataTableResponse<Array<EventDetails>>>) => data.response)
      );
  }

  removeEvent(eventId: string): Observable<Boolean> {
    return this._httpClient
      .get<ApiResult<Boolean>>(
        `${environment.apiUrl}event/remove?eventId=${encodeURIComponent( eventId)}  `,{ headers: this._https_header }).pipe(
          map((data: ApiResult<Boolean>) => data.response));
  }

  campaignEventList(filter: CampaignEventFilter): Observable<DataTableResponse<Array<CampaignsDetails>>> {
    return this._httpClient
      .post<ApiResult<DataTableResponse<Array<CampaignsDetails>>>>(`${environment.apiUrl}campaign/event/list`, filter, { headers: this._https_header }).pipe(
         map( (data: ApiResult<DataTableResponse<Array<CampaignsDetails>>>) => data.response)
      );
  }


}
