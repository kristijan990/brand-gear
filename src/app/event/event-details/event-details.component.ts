import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { CampaignsDetails } from 'src/app/campaign/campaign.model';
import { DataTableResponse } from 'src/app/shared/shared.model';
import { CampaignEventFilter, EventDetails } from '../event.model';
import { EventService } from '../event.service';

@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.scss'],
})
export class EventDetailsComponent implements OnInit {
  private _unsubscribeAll: Subject<void> = new Subject<void>();
  activeGuid: string = null;
  showSpinner: Boolean = false;
  is_loaded: Boolean = false;
  has_campaign: Boolean = false;
  eventDetails: EventDetails = new EventDetails();
  filter: CampaignEventFilter = new CampaignEventFilter();
  campaignDetailsList: Array<CampaignsDetails> = new Array<CampaignsDetails>();

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _eventService: EventService
  ) {}

  ngOnInit(): void {
    this._activatedRoute.params
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((params: Params) => {
        console.log('params -> ', params);

        if (params['guid']) {
          this.activeGuid = params['guid'];
          console.log('activeGuid', this.activeGuid);
          this.showSpinner = true;
          this.getEventDetailS();
        }
      });
  }

  getEventDetailS() {
    this._eventService
      .getEventDetails(this.activeGuid)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((data: EventDetails) => {
        this.eventDetails = data;

        if (this.eventDetails.eventCampaigns) {
          this.has_campaign = true;
        } else {
          this.has_campaign = false;
        }
        this.is_loaded = true;

        this.getCampaignList();
      });
  }

  // API za listanje na Campanjite

  getCampaignList() {
    this.filter.pageNumber = 1;
    this.filter.pageSize = 10;
    this.filter.sortDirection = 'asc';
    this.filter.sortColumn = '';
    this.filter.eventId = this.eventDetails.eventId;

    this._eventService
      .campaignEventList(this.filter)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((data: DataTableResponse<Array<CampaignsDetails>>) => {
        this.campaignDetailsList = data.data;
        this.showSpinner = false;
      });
  }
}
