import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {

  images = [
    'https://images.pexels.com/photos/595747/pexels-photo-595747.jpeg?auto=compress&amp;cs=tinysrgb&amp;h=750&amp;w=1260',
    'https://images.pexels.com/photos/1029039/pexels-photo-1029039.jpeg?auto=compress&amp;cs=tinysrgb&amp;h=750&amp;w=1260',
    'https://images.pexels.com/photos/983436/pexels-photo-983436.jpeg?auto=compress&amp;cs=tinysrgb&amp;h=750&amp;w=1260',
    'https://images.pexels.com/photos/1028437/pexels-photo-1028437.jpeg?auto=compress&amp;cs=tinysrgb&amp;h=750&amp;w=1260',
    'https://images.pexels.com/photos/963436/pexels-photo-963436.jpeg?auto=compress&amp;cs=tinysrgb&amp;h=750&amp;w=1260',
    'https://images.pexels.com/photos/1028599/pexels-photo-1028599.jpeg?auto=compress&amp;cs=tinysrgb&amp;h=750&amp;w=1260',
    'https://images.pexels.com/photos/814372/pexels-photo-814372.jpeg?auto=compress&amp;cs=tinysrgb&amp;h=750&amp;w=1260',
    'https://images.pexels.com/photos/1018797/pexels-photo-1018797.jpeg?auto=compress&amp;cs=tinysrgb&amp;h=750&amp;w=1260',
    'https://images.pexels.com/photos/830912/pexels-photo-830912.jpeg?auto=compress&amp;cs=tinysrgb&amp;h=750&amp;w=1260',
    'https://images.pexels.com/photos/890500/pexels-photo-890500.jpeg?auto=compress&amp;cs=tinysrgb&amp;h=750&amp;w=1260',
    'https://images.pexels.com/photos/863985/pexels-photo-863985.jpeg?auto=compress&amp;cs=tinysrgb&amp;h=750&amp;w=1260',
    'https://images.pexels.com/photos/398549/pexels-photo-398549.jpeg?auto=compress&amp;cs=tinysrgb&amp;h=750&amp;w=1260',
    'https://images.pexels.com/photos/1024252/pexels-photo-1024252.jpeg?auto=compress&amp;cs=tinysrgb&amp;h=750&amp;w=1260',
    'https://images.pexels.com/photos/91216/pexels-photo-91216.jpeg?auto=compress&amp;cs=tinysrgb&amp;h=750&amp;w=1260',
    'https://images.pexels.com/photos/1018350/pexels-photo-1018350.jpeg?auto=compress&amp;cs=tinysrgb&amp;h=750&amp;w=1260',]

  constructor() { }

  ngOnInit(): void {
  }

}
