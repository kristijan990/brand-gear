import { Component, Injectable, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Subject, takeUntil } from 'rxjs';
import { CampaignsDetails } from '../campaign/campaign.model';
import { CampaignService } from '../campaign/campaign.service';
import { EventDetails, EventFilter } from '../event/event.model';
import { EventService } from '../event/event.service';
import { DataTableFilter, DataTableResponse } from '../shared/shared.model';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss'],
})
export class LandingPageComponent implements OnInit {
  eventList: EventDetails[];
  filter: EventFilter = new EventFilter();
  showSpinner: Boolean = false;
  private _unsubscribeAll: Subject<void> = new Subject<void>();
  @Input() list: EventDetails;
  is_company: boolean = false;
  campaignsList: Array<CampaignsDetails> = new Array<CampaignsDetails>();

  filterCampaign: DataTableFilter = new DataTableFilter();
  campaignList: CampaignsDetails[];

  constructor(
    private _eventService: EventService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _campaignService: CampaignService
  ) {}

  ngOnInit(): void {
    this.getEventList();
    this.getCampaignList();
  }

  getEventList() {
    this.filter.pageNumber = 1;
    this.filter.pageSize = 3;
    this.filter.sortDirection = 'asc';
    this.filter.sortColumn = '';

    this._eventService
      .listEvent(this.filter)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((data: DataTableResponse<Array<EventDetails>>) => {
        this.eventList = data.data;
        console.log('event list ---->', this.eventList);

        // this._sharedService.is_saving_results.next(true);
        this.showSpinner = false;
      });
  }

  getCampaignList(): void {
    this.filterCampaign.pageNumber = 1;
    this.filterCampaign.pageSize = 6;
    this.filterCampaign.sortDirection = 'asc';
    this.filterCampaign.sortColumn = 'campaignId';

    this._campaignService
      .listCampaign(this.filterCampaign)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((data: DataTableResponse<Array<CampaignsDetails>>) => {
        this.campaignsList = data.data;
        console.log('campaigh list ---->', this.campaignsList);
        // this._sharedService.is_saving_results.next(true);
        this.showSpinner = false;
      });
  }

  loadCampaign() {
    this.showSpinner = true;
    this._campaignService
      .listCampaign(this.filterCampaign)
      .subscribe((response: any) => {
        this.campaignList = response.data;
        this.showSpinner = false;
      });
  }

  viewEventList() {
    this._router.navigate(['/event/list']);
  }
}
