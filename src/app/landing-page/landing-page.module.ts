import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingPageRoutingModule } from './landing-routing.module';
import { LandingPageComponent } from './landing-page.component';
import { EventCardViewComponent } from '../event/event-card-view/event-card-view.component';
import { EventModule } from '../event/event.module';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgxMasonryModule } from 'ngx-masonry';
import { CampaignModule } from '../campaign/campaign.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [LandingPageComponent],
  imports: [
    CommonModule,
    LandingPageRoutingModule,
    EventModule,
    NgxMasonryModule,
    CampaignModule,
    SharedModule,
  ],
})
export class LandingPageModule {}
