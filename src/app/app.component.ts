import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { AccountObject } from './auth/auth.model';
import { SharedService } from './shared/shared.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'brand-gear-app';

  isLogin: Boolean = false;
  activeAccount = new AccountObject();
  is_loaded: Boolean = false;
  wrongpassword: Boolean = false;
  private _unsubscribeAll: Subject<void>  = new Subject<void>();

  constructor(
             private _router: Router,
             private _sharedService: SharedService
             ) {
              this._router.events.subscribe((val) => {
                if(val instanceof NavigationEnd){
                  this.isLogin = val.urlAfterRedirects.startsWith('/auth/');
                }
            });
  }

  ngOnInit(){

    this.isLoaded();

  }

  isLoaded() {

    this._sharedService.is_loaded.pipe(takeUntil(this._unsubscribeAll)).subscribe(
      (data: Boolean) => {
        this.is_loaded = !data;

      }

    )

  }

  // wrongPassword(): void {
  //   this._sharedService.wrongPassword.pipe(takeUntil(this._unsubscribeAll)).subscribe(
  //     (data: Boolean) => {
  //       this.wrongpassword = data;

  //     }
  //   )
  // }

}
